<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public function employee(){
        return $this->belongsTo('App\Employee');
    }
    public function overhead(){
        return $this->belongsTo('App\Overhead');
    }
}
