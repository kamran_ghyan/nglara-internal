<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function employee(){
        return $this->belongsTo('App\Employee');
    }
}
