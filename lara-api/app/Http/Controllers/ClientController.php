<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ClientController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        //$this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // Set limit for user input
        $limit = $request->input('limit')?$request->input('limit'):200;

        // Get list of location
        $clients = Client::orderBy('name', 'asc')->paginate($limit);

        // Append limit
        $clients->appends(array(
            'limit' => $limit
        ));

        // Convert objects into Array
        $clientsArray = $clients->toArray();

        // Response
        return response()->json([
            'total'         => $clientsArray['total'],
            'per_page'      => $clientsArray['per_page'],
            'current_page'  => $clientsArray['current_page'],
            'last_page'     => $clientsArray['last_page'],
            'next_page_url' => $clientsArray['next_page_url'],
            'prev_page_url' => $clientsArray['prev_page_url'],
            'from'          => $clientsArray['from'],
            'to'            => $clientsArray['to'],
            'data'          => $clientsArray['data']
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = new Client;
        $client->name = $request->name;
        $client->save();

        return response()->json(['message' => 'Client added successfully'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get item
        $client = Client::find($id);

        // Response
        return response()->json([
            $client
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::find($id);

        $client->name = $request->name;

        $client->save();

        return response()->json(['message' => 'Client edit successfully'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find
        $client = Client::find($id);

        // Delete
        $client->delete();

        return response()->json(['message' => 'Client delete successfully'], 200);
    }
}
