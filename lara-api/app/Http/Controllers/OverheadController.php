<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Overhead;
use App\Location;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class OverheadController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        //$this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // Set limit for user input
        $limit = $request->input('limit')?$request->input('limit'):200;

        // Get list of employee
        $location = Location::all();

        $overheads = Overhead::with(['location'])->orderBy('name', 'asc')->paginate($limit);

        // Get list of location
        //$overheads = Overhead::orderBy('id', 'desc')->paginate($limit);

        // Append limit
        $overheads->appends(array(
            'limit' => $limit
        ));

        // Convert objects into Array
        $overheadsArray = $overheads->toArray();

        // Response
        return response()->json([
            'total'         => $overheadsArray['total'],
            'per_page'      => $overheadsArray['per_page'],
            'current_page'  => $overheadsArray['current_page'],
            'last_page'     => $overheadsArray['last_page'],
            'next_page_url' => $overheadsArray['next_page_url'],
            'prev_page_url' => $overheadsArray['prev_page_url'],
            'from'          => $overheadsArray['from'],
            'to'            => $overheadsArray['to'],
            'data'          => $overheadsArray['data']
        ], 200);
    }

    public function getSum(Request $request)
    {

        $totalSum = DB::table('overheads')->sum('cost');
        $totalSumUsd = DB::table('overheads')->sum('usd_cost');

        return response()->json([
            'total'=> $totalSum,
            'total_usd' => $totalSumUsd
        ], 200);
    }

    public function getSumByLocation(Request $request, $id)
    {

        $count = DB::table('overheads')->where([['location_id', '=', $id],])->count();
        if($id == 1){
            $totalSum = DB::table('overheads')->where([['location_id', '=', $id],])->sum('usd_cost');
        } else {
            $totalSum = DB::table('overheads')->where([['location_id', '=', $id],])->sum('cost');
        }


        return response()->json([
            'count'=> $count,
            'total'=> $totalSum
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $overhead = new Overhead;
        if($request->usd_cost){
            $overhead->name         = $request->name;
            $overhead->usd_cost         = $request->usd_cost;
            $overhead->location_id     = $request->location_id;
        } else {
            $overhead->name         = $request->name;
            $overhead->cost         = $request->cost;
            $overhead->location_id     = $request->location_id;
        }

        $overhead->save();

        return response()->json(['message' => 'Overhead added successfully'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get item
        $overhead = Overhead::find($id);

        // Response
        return response()->json([
            $overhead
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Get item
        $overhead = Overhead::find($id);

        // Response
        return response()->json([
            $overhead
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->name);
        $overhead = Overhead::find($id);
        if($request->usd_cost){
            $overhead->name         = $request->name;
            $overhead->usd_cost     = $request->usd_cost;
            $overhead->cost         = 0;
            $overhead->location_id  = $request->location_id;
        } else {
            $overhead->name         = $request->name;
            $overhead->cost         = $request->cost;
            $overhead->usd_cost     = 0;
            $overhead->location_id  = $request->location_id;
        }



        $overhead->save();

        return response()->json(['message' => 'Overhead edited successfully'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find
        $overhead = Overhead::find($id);

        // Delete
        $overhead->delete();

        return response()->json(['message' => 'Overhead delete successfully'], 200);
    }

    public function search(Request $request){

        // Set limit for user input
        $limit = $request->input('limit')?$request->input('limit'):200;

        $location_id    = $request->location;

        if($location_id) {
            // Get list of employee
            $location = Location::all();

            // Get list
            $employees = Overhead::with([
                'location' => function ($query) use ($location_id) {
                    $query->where('id', $location_id);
                }
            ])
                ->where([
                    ['location_id', '=', $location_id],

                ])
                ->orderBy('id', 'desc')->paginate($limit);

            // Append limit
            $employees->appends(array(
                'limit' => $limit
            ));

            // Convert objects into Array
            $employeesArray = $employees->toArray();

            // Response
            return response()->json([
                'total' => $employeesArray['total'],
                'per_page' => $employeesArray['per_page'],
                'current_page' => $employeesArray['current_page'],
                'last_page' => $employeesArray['last_page'],
                'next_page_url' => $employeesArray['next_page_url'],
                'prev_page_url' => $employeesArray['prev_page_url'],
                'from' => $employeesArray['from'],
                'to' => $employeesArray['to'],
                'data' => $employeesArray['data']
            ], 200);
        }  else{

            // Get list of employee
            $location = Location::all();

            $employees = Overhead::with([ 'location'])->orderBy('id', 'desc')->paginate($limit);

            // Append limit
            $employees->appends(array(
                'limit' => $limit
            ));

            // Convert objects into Array
            $employeesArray = $employees->toArray();

            // Response
            return response()->json([
                'total'         => $employeesArray['total'],
                'per_page'      => $employeesArray['per_page'],
                'current_page'  => $employeesArray['current_page'],
                'last_page'     => $employeesArray['last_page'],
                'next_page_url' => $employeesArray['next_page_url'],
                'prev_page_url' => $employeesArray['prev_page_url'],
                'from'          => $employeesArray['from'],
                'to'            => $employeesArray['to'],
                'data'          => $employeesArray['data']
            ], 200);
        }




    }
}
