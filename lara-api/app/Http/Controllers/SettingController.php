<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class SettingController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        //$this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    /**
     * Get request option
     *
     * @return \Illuminate\Http\Response
     */
    public function getOption(Request $request, $option){
        // Get item
        $setting = Setting::where('name', '=', $option)->first();

        // Response
        return response()->json([
            $setting
        ], 200);
    }

    public function update(Request $request, $id){

        // Get item

        //$setting = Setting::where('name', '=', $request->option)->first();
        //$setting->value = $request->total_hours;
        //$setting->save();

        if($request->option === 'total_hours'){
            DB::table('settings')
                ->where('name', $request->option)  // find your user by their email
                ->limit(1)  // optional - to ensure only one record is updated.
                ->update(array('value' => $request->total_hours));  // update the record in the DB.
        }
        if($request->option === 'api_key'){
            DB::table('settings')
                ->where('name', $request->option)  // find your user by their email
                ->limit(1)  // optional - to ensure only one record is updated.
                ->update(array('value' => $request->api_key));  // update the record in the DB.
        }

        // Response

        return response()->json(['message' => 'Setting updated successfully'], 200);
    }

    public function updateOption(Request $request){

        // Get item
        //$setting = Setting::where('name', '=', $option)->first();

        // Response
        return response()->json([
            $request
        ], 200);
    }


}
