<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Client;
use App\Department;
use App\Employee;
use App\Setting;
use App\Location;
use App\Overhead;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class EmployeeController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        //$this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // Set limit for user input
        $limit = $request->input('limit')?$request->input('limit'):200;

        // Get list of employee
        $department = Department::all();
        $location = Location::all();
        $client = Client::all();

        $employees = Employee::with(['type', 'client', 'location'])->orderBy('name', 'asc')->paginate($limit);

        $supportSalary = Employee::sum('salary');
        // Append limit
        $employees->appends(array(
            'limit' => $limit
        ));

        // Convert objects into Array
        $employeesArray = $employees->toArray();

        $gross = array();
        $net = array();
        $i = 0;

        foreach( $employeesArray['data'] as $employee) {

            $totalHours = Setting::where('name', '=', 'total_hours')->first();
            $exchangeRate = Setting::where('name', '=', 'api_key')->first();


            if($employee['hours_worked'] == 0){
                $gross = $employee['rate'] * $totalHours['value'];
            } else {
                $gross = $employee['rate'] * $employee['hours_worked'] ;
            }

            if($employee['salary'] != 0){
                $net = $gross - $employee['salary'] / $exchangeRate['value'] ;
            } else {
                $net = $gross - $employee['gross'];
                if($net > 0) {
                } else {
                    $net = 0;
                }
            }

            $this->array_insert(
                $employeesArray['data'][$i],
                'gender',
                [
                    'net_' => $net
                ]);

            $this->array_insert(
                $employeesArray['data'][$i],
                'gender',
                [
                    'gross_' => $gross
                ]);

            $i++;
        }

        // Response
        return response()->json([
            'total'         => $employeesArray['total'],
            'per_page'      => $employeesArray['per_page'],
            'current_page'  => $employeesArray['current_page'],
            'last_page'     => $employeesArray['last_page'],
            'next_page_url' => $employeesArray['next_page_url'],
            'prev_page_url' => $employeesArray['prev_page_url'],
            'from'          => $employeesArray['from'],
            'to'            => $employeesArray['to'],
            'sumSalary'     => $supportSalary,
            'data'          => $employeesArray['data']
        ], 200);
    }

    public function array_insert(&$array, $position, $insert)
    {
        if (is_int($position)) {
            array_splice($array, $position, 0, $insert);
        } else {
            $pos   = array_search($position, array_keys($array));
            $array = array_merge(
                array_slice($array, 0, $pos),
                $insert,
                array_slice($array, $pos)
            );
        }
    }

    public function location(Request $request, $id)
    {
        // Get list
        $employees = Employee::with([
            'location'=> function($query) use ($id) {
                $query->where('id', $id);
            }
        ])
            ->where([
                ['location_id', '=', $id],
            ])
            ->get();

        $income = Employee::where([
            ['type__id', '=', 1],
            ['type_id', '!=', 9],
            ['exclude', '!=', 1],
            ['location_id', '=', $id]
        ])->count();

        $support = Employee::where([
            ['type__id', '=', 2],
            ['type_id', '!=', 9],
            ['exclude', '!=', 1],
            ['location_id', '=', $id]
        ])->count();

        $bench = Employee::where([
            ['type_id', '=', 9],
            ['exclude', '!=', 1],
            ['location_id', '=', $id]
        ])->count();

        if($id == 1) {
            $incomeSalary  = Employee::where([
                ['type__id', '=', 1],
                ['exclude', '!=', 1],
                ['location_id', '=', $id]
            ])->sum('gross');

            $supportSalary = Employee::where([
                ['type__id', '=', 2],
                ['exclude', '!=', 1],
                ['location_id', '=', $id]
            ])->sum('gross');

            $benchSalary = Employee::where([
                ['type_id', '=', 9],
                ['exclude', '!=', 1],
                ['location_id', '=', $id]
            ])->sum('gross');
        } else {
            $incomeSalary  = Employee::where([
                ['type__id', '=', 1],
                ['exclude', '!=', 1],
                ['location_id', '=', $id]
            ])->sum('salary');

            $supportSalary = Employee::where([
                ['type__id', '=', 2],
                ['exclude', '!=', 1],
                ['location_id', '=', $id]
            ])->sum('salary');

            $benchSalary = Employee::where([
                ['type_id', '=', 9],
                ['exclude', '!=', 1],
                ['location_id', '=', $id]
            ])->sum('salary');
        }




        // Response
        return response()->json([
            'income'    => $income,
            'support'   => $support,
            'bench'     => $bench,
            'incomeSalary'  => $incomeSalary,
            'supportSalary' => $supportSalary,
            'benchSalary'   => $benchSalary,
            'data' => $employees
        ], 200);
    }

    public function locations(Request $request)
    {
        // Set limit for user input
        $limit = $request->input('limit')?$request->input('limit'):200;
        // Get list
        $location = Location::all();

        $employees = Employee::with(['location'])->orderBy('name', 'asc')->paginate($limit);

//        $income = Employee::where([
//            ['type__id', '=', 1],
//            ['type_id', '!=', 9],
//            ['exclude', '!=', 1],
//            ['location_id', '=', $id]
//        ])->count();
//
//        $support = Employee::where([
//            ['type__id', '=', 2],
//            ['type_id', '!=', 9],
//            ['exclude', '!=', 1],
//            ['location_id', '=', $id]
//        ])->count();
//
//        $bench = Employee::where([
//            ['type_id', '=', 9],
//            ['exclude', '!=', 1],
//            ['location_id', '=', $id]
//        ])->count();
//
//        $incomeSalary  = Employee::where([
//            ['type__id', '=', 1],
//            ['exclude', '!=', 1],
//            ['location_id', '=', $id]
//        ])->sum('salary');
//
//        $supportSalary = Employee::where([
//            ['type__id', '=', 2],
//            ['exclude', '!=', 1],
//            ['location_id', '=', $id]
//        ])->sum('salary');
//
//        $benchSalary = Employee::where([
//            ['type_id', '=', 9],
//            ['exclude', '!=', 1],
//            ['location_id', '=', $id]
//        ])->sum('salary');

        // Response
        return response()->json([
//            'income'    => $income,
//            'support'   => $support,
//            'bench'     => $bench,
//            'incomeSalary'  => $incomeSalary,
//            'supportSalary' => $supportSalary,
//            'benchSalary'   => $benchSalary,
            'data' => $employees
        ], 200);
    }

    public function locationType(Request $request, $id, $type_id)
    {

        // Set limit for user input
        $limit = $request->input('limit')?$request->input('limit'):200;

        if($type_id == 3){
            $overhead = Overhead::with(['location'])->where([
                ['location_id', '=', $id],
            ])->orderBy('id', 'desc')->paginate($limit);

            $overheadExpense = Overhead::sum('cost');

            // Append limit
            $overhead->appends(array(
                'limit' => $limit
            ));

            // Convert objects into Array
            $overheadArray = $overhead->toArray();



            $gross = array();
            $net = array();
            $i = 0;

            foreach( $overheadArray['data'] as $employee) {

                $totalHours = Setting::where('name', '=', 'total_hours')->first();
                $exchangeRate = Setting::where('name', '=', 'api_key')->first();


                if($employee['hours_worked'] == 0){
                    $gross = $employee['rate'] * $totalHours['value'];
                } else {
                    $gross = $employee['rate'] * $employee['hours_worked'] ;
                }

                if($employee['salary'] != 0){
                    $net = $gross - $employee['salary'] / $exchangeRate['value'] ;
                } else {
                    $net = $gross - $employee['gross'];
                    if($net > 0) {
                    } else {
                        $net = 0;
                    }
                }

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'net_' => $net
                    ]);

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'gross_' => $gross
                    ]);

                $i++;
            }

            // Response
            return response()->json([
                'total'         => $overheadArray['total'],
                'per_page'      => $overheadArray['per_page'],
                'current_page'  => $overheadArray['current_page'],
                'last_page'     => $overheadArray['last_page'],
                'next_page_url' => $overheadArray['next_page_url'],
                'prev_page_url' => $overheadArray['prev_page_url'],
                'from'          => $overheadArray['from'],
                'to'            => $overheadArray['to'],
                'sumSalary'     => $overheadExpense,
                'data'          => $overheadArray['data']
            ], 200);

        }
        else if($type_id == 9) {

            // Get list of employee
            $department = Department::all();
            $location = Location::all();
            $type = Type::all();
            $client = Client::all();
            $overhead = Overhead::all();

            $employees = Employee::with(['type', 'client', 'location'])->where([
                ['location_id', '=', $id],
                ['type_id', '=', $type_id],
            ])->orderBy('id', 'desc')->paginate($limit);

            $supportSalary = Employee::sum('salary');

            // Append limit
            $employees->appends(array(
                'limit' => $limit
            ));

            // Convert objects into Array
            $employeesArray = $employees->toArray();

            $gross = array();
            $net = array();
            $i = 0;

            foreach( $employeesArray['data'] as $employee) {

                $totalHours = Setting::where('name', '=', 'total_hours')->first();
                $exchangeRate = Setting::where('name', '=', 'api_key')->first();


                if($employee['hours_worked'] == 0){
                    $gross = $employee['rate'] * $totalHours['value'];
                } else {
                    $gross = $employee['rate'] * $employee['hours_worked'] ;
                }

                if($employee['salary'] != 0){
                    $net = $gross - $employee['salary'] / $exchangeRate['value'] ;
                } else {
                    $net = $gross - $employee['gross'];
                    if($net > 0) {
                    } else {
                        $net = 0;
                    }
                }

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'net_' => $net
                    ]);

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'gross_' => $gross
                    ]);

                $i++;
            }

            // Response
            return response()->json([
                'total'         => $employeesArray['total'],
                'per_page'      => $employeesArray['per_page'],
                'current_page'  => $employeesArray['current_page'],
                'last_page'     => $employeesArray['last_page'],
                'next_page_url' => $employeesArray['next_page_url'],
                'prev_page_url' => $employeesArray['prev_page_url'],
                'from'          => $employeesArray['from'],
                'to'            => $employeesArray['to'],
                'sumSalary'     => $supportSalary,
                'data'          => $employeesArray['data']
            ], 200);
        }
        else {

            // Get list of employee
            $department = Department::all();
            $type = Type::all();
            $location = Location::all();
            $client = Client::all();
            $overhead = Overhead::all();

            $employees = Employee::with(['type', 'client', 'location'])->where([
                ['location_id', '=', $id],
                ['type__id', '=', $type_id],
                ['type_id', '!=', 9],
            ])->orderBy('id', 'desc')->paginate($limit);

            $supportSalary = Employee::sum('salary');

            // Append limit
            $employees->appends(array(
                'limit' => $limit
            ));

            // Convert objects into Array
            $employeesArray = $employees->toArray();



            $gross = array();
            $net = array();
            $i = 0;

            foreach( $employeesArray['data'] as $employee) {

                $totalHours = Setting::where('name', '=', 'total_hours')->first();
                $exchangeRate = Setting::where('name', '=', 'api_key')->first();


                if($employee['hours_worked'] == 0){
                    $gross = $employee['rate'] * $totalHours['value'];
                } else {
                    $gross = $employee['rate'] * $employee['hours_worked'] ;
                }

                if($employee['salary'] != 0){
                    $net = $gross - $employee['salary'] / $exchangeRate['value'] ;
                } else {
                    $net = $gross - $employee['gross'];
                    if($net > 0) {
                    } else {
                        $net = 0;
                    }
                }

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'net_' => $net
                    ]);

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'gross_' => $gross
                    ]);

                $i++;
            }

            // Response
            return response()->json([
                'total'         => $employeesArray['total'],
                'per_page'      => $employeesArray['per_page'],
                'current_page'  => $employeesArray['current_page'],
                'last_page'     => $employeesArray['last_page'],
                'next_page_url' => $employeesArray['next_page_url'],
                'prev_page_url' => $employeesArray['prev_page_url'],
                'from'          => $employeesArray['from'],
                'to'            => $employeesArray['to'],
                'sumSalary'     => $supportSalary,
                'data'          => $employeesArray['data']
            ], 200);
        }
    }

    /**
     * Export data from DB and save into CSV.
     *
     * @return \Illuminate\Http\Response
     */
    public function export(){


        //$excel = App::make('excel');

        Excel::create('Filename', function($excel) {

            // Set the title
            $excel->setTitle('Our new awesome title');

            // Chain the setters
            $excel->setCreator('Maatwebsite')
                ->setCompany('Maatwebsite');

            // Call them separately
            $excel->setDescription('A demonstration to change the file properties');

        })->download('xls');

//        $headers = array(
//            'Content-Type'        => 'application/vnd.ms-excel; charset=utf-8',
//            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
//            'Content-Disposition' => 'attachment; filename=employees-report.csv',
//            'Expires'             => '0',
//            'Pragma'              => 'public',
//        );
//
//        $d = time();
//        $name = date("Y-m-d h:i:s", $d);
//        $filename = $name .'.csv';
//        $handle = fopen($filename, 'w');
//        fputcsv($handle, [
//            "id",
//            "Name",
//            "Rate",
//            "PKR Salary",
//            "USD Salary",
//        ]);
//
//        DB::table("employees")->orderBy('name', 'asc')->chunk(100, function($data) use ($handle) {
//            foreach ($data as $row) {
//                // Add a new row with data
//                fputcsv($handle, [
//                    $row->id,
//                    $row->name,
//                    $row->rate,
//                    $row->salary,
//                    $row->gross,
//                ]);
//            }
//        });
//
//        fclose($handle);
//
//        return response()->download($filename, $filename, $headers);

    }

    /**
     * Import data from CSV and save into Database.
     *
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request){

        $destination_path = base_path().'/public/uploads/';
        $inputs = $request->all();
        //print_r($inputs['file']->getClientOriginalName());
        $inputs['file'] = $destination_path.$inputs['file']->getClientOriginalName();
        $request->file('file')->move($destination_path, $inputs['file']);

        $filename = $inputs['file'];
        $file = fopen($filename,'r');

        $i = 0;
        while(! feof($file))
        {
            $employee_data = fgetcsv($file);
            if($i == 0){

            } else {
                // Get values from CSV file
                $id         = $employee_data[0];
                $name       = $employee_data[1];
                $rate       = $employee_data[2];
                $pkr_salary = $employee_data[3];
                $usd_salary = $employee_data[4];

                if($id != null && $name != null && $rate != null && $pkr_salary != null && $usd_salary != null){
                    // Find employee by ID
                    $employee = Employee::find($id);

                    // Set Values
                    $employee->rate = $rate;
                    $employee->salary = $pkr_salary;
                    $employee->gross = $usd_salary;

                    $employee->save();
                }
            }

            $i++;
        }

        fclose($file);

        return response()->json(['message' => 'CSV imported successfully'], 200);

    }

    public function reset(Request $request){
        $employees = Employee::all();

        $rate = 10;
        $pkr_salary = 50000;
        $usd_salary = 500;

        foreach ($employees as $employee){
            if($employee->rate != 0){
                $employee->rate = $rate;
            }

            if($employee->salary !=0){
                $employee->salary = $pkr_salary;
            }
            if($employee->gross != 0 && $employee->salary ==0){
                $employee->gross = $usd_salary;
            }

            $employee->save();
        }

        return response()->json(['message' => 'Employee data reset successfully'], 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = DB::table('types')->where('id', $request->emp_type)->first();

        $employee = new Employee;
        if($request->usdsalary){
            $employee->name         = $request->name;
            //$employee->salary       = $request->salary;
            $employee->gross        = $request->usdsalary;
            //$employee->net          = $request->net;
            $employee->rate         = $request->rate;
            $employee->type__id      = $type->type;
            $employee->type_id     = $request->emp_type;
            $employee->client_id    = $request->client_id;
//        $employee->department_id = $request->department_id;
            $employee->location_id  = $request->location_id;
            $employee->hours_worked  = $request->hours_worked;
            $employee->notes  = $request->notes;
        } else {
            $employee->name         = $request->name;
            $employee->salary       = $request->salary;
            //$employee->gross        = $request->gross;
            //$employee->net          = $request->net;
            $employee->rate         = $request->rate;
            $employee->type__id      = $type->type;
            $employee->type_id     = $request->emp_type;
            $employee->client_id    = $request->client_id;
//        $employee->department_id = $request->department_id;
            $employee->location_id  = $request->location_id;
            $employee->hours_worked  = $request->hours_worked;
            $employee->notes  = $request->notes;
        }


        //dd($employee);
        $employee->save();

        return response()->json(['message' => 'Employee added successfully'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get item
        $employee = Employee::find($id);

        // Response
        return response()->json([
            $employee
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Get item
        $employee = Employee::find($id);

        // Response
        return response()->json([
            $employee
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = DB::table('types')->where('id', $request->emp_type)->first();

        $employee = Employee::find($id);

        if($request->usdsalary){

            $employee->name = $request->name;
            $employee->notes = $request->notes;
            $employee->gross = $request->usdsalary;
            $employee->salary = 0;
            //$employee->gross = $request->gross;
            //$employee->net = $request->net;
            $employee->rate = $request->rate;


            if($request->location_id == null){
            } else{
                $employee->location_id = $request->location_id;
            }

            if($request->client_id == null){
            } else{
                $employee->client_id = $request->client_id;
            }

            $employee->type_id = $request->emp_type;
            $employee->type__id = $type->type;

            $employee->hours_worked = $request->hours_worked;

        } else {
            $employee->name = $request->name;
            $employee->notes = $request->notes;
            $employee->salary = $request->salary;
            $employee->gross = 0;
            //$employee->net = $request->net;
            $employee->rate = $request->rate;


            if($request->location_id == null){
            } else{
                $employee->location_id = $request->location_id;
            }

            if($request->client_id == null){
            } else{
                $employee->client_id = $request->client_id;
            }

            $employee->type_id = $request->emp_type;
            $employee->type__id = $type->type;

            $employee->hours_worked = $request->hours_worked;
        }



        //dd($type);
        $employee->save();

        return response()->json(['message' => $employee], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateSalary(Request $request, $id)
    {
        $employee = Employee::find($id);

        if($request->usdsalary){
            $employee->gross = $request->usdsalary;
            $employee->salary = 0;
        } else {
            $employee->salary = $request->salary;
            $employee->gross = 0;
        }


        $employee->save();

        return response()->json(['message' => 'Employee edit successfully'], 200);
    }

    public function updateRate(Request $request, $id)
    {
        $employee = Employee::find($id);

        $employee->rate = $request->rate;

        $employee->save();

        return response()->json(['message' => 'Employee hourly rate edit successfully'], 200);
    }

    public function updateExclude(Request $request, $id)
    {
        $employee = Employee::find($id);

        $employee->exclude = $request->exclude;

        $employee->save();

        return response()->json(['message' => 'Employee edit successfully'], 200);
    }

    public function updateToggle(Request $request, $id)
    {
        $employee = Employee::find($id);

        $employee->toggle = $request->toggle;

        $employee->save();

        return response()->json(['message' => 'Employee edit successfully'], 200);
    }

    public function allToggle(Request $request)
    {


        $checkToggle = DB::table('employees')->get(['toggle',]);
        //dd($checkToggle[1]->toggle);

        if($checkToggle[0]->toggle == 0 || $checkToggle[1]->toggle == 0 || $checkToggle[2]->toggle == 0){
            $updateEmployee = DB::table('employees')->update(array('toggle' => 1));
        } else {
            $updateEmployee = DB::table('employees')->update(array('toggle' => 0));
        }

        $data = DB::table('employees')->get(['toggle',]);
        $data = $data[0]->toggle;

        return response()->json([
            'message' => 'Updated toggle state',
            'data' => $data
        ], 200);
    }


    public function getType(Request $request, $id){
        // Get item
        $employee = Employee::where('type_id', '=', $id)->get()->count();
        // Response
        return response()->json([
            $employee
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find
        $client = Employee::find($id);

        // Delete
        $client->delete();

        return response()->json(['message' => 'Employee delete successfully'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function allCrm()
    {
        // Get list of location
        $employees = Employee::where('type_id', '=', 1)->first();

        // Response
        return response()->json([
            $employees
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function allManager()
    {
        // Get list of location
        $employees = Employee::where('type_id', '=', 2)->first();

        // Response
        return response()->json([
            $employees
        ], 200);
    }



    public function search(Request $request){

        // Set limit for user input
        $limit = $request->input('limit')?$request->input('limit'):200;

        $type_id  = $request->type;
        $location_id    = $request->location;
        $client_id    = $request->client;

        //$department = Department::find($department_id)->departments;

        if($request->type && $request->location && $request->client){

            // Get list
            $employee = Employee::with([
                'type'=> function($query) use ($type_id) {
                    $query->where('id', $type_id);
                },
                'location'=> function($query) use ($location_id) {
                    $query->where('id', $location_id);
                },
                'client'=> function($query) use ($client_id) {
                    $query->where('id', $client_id);
                }
            ])
                ->where([
                    ['type_id', '=', $type_id],
                    ['location_id', '=', $location_id],
                    ['client_id', '=', $client_id],
                ])
                ->orderBy('id', 'desc')->paginate($limit);

            // Append limit
            $employee->appends(array(
                'limit' => $limit
            ));

            // Convert objects into Array
            $employeesArray = $employee->toArray();

            $gross = array();
            $net = array();
            $i = 0;

            foreach( $employeesArray['data'] as $employee) {

                $totalHours = Setting::where('name', '=', 'total_hours')->first();
                $exchangeRate = Setting::where('name', '=', 'api_key')->first();


                if($employee['hours_worked'] == 0){
                    $gross = $employee['rate'] * $totalHours['value'];
                } else {
                    $gross = $employee['rate'] * $employee['hours_worked'] ;
                }

                if($employee['salary'] != 0){
                    $net = $gross - $employee['salary'] / $exchangeRate['value'] ;
                } else {
                    $net = $gross - $employee['gross'];
                    if($net > 0) {
                    } else {
                        $net = 0;
                    }
                }

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'net_' => $net
                    ]);

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'gross_' => $gross
                    ]);

                $i++;
            }


            // Response
            return response()->json([
                'total'         => $employeesArray['total'],
                'per_page'      => $employeesArray['per_page'],
                'current_page'  => $employeesArray['current_page'],
                'last_page'     => $employeesArray['last_page'],
                'next_page_url' => $employeesArray['next_page_url'],
                'prev_page_url' => $employeesArray['prev_page_url'],
                'from'          => $employeesArray['from'],
                'to'            => $employeesArray['to'],
                'data'          => $employeesArray['data']
            ], 200);

        }
        elseif($request->type && $request->location){

            // Get list
            $employee = Employee::with([
                'type'=> function($query) use ($type_id) {
                    $query->where('id', $type_id);
                },
                'location'=> function($query) use ($location_id) {
                    $query->where('id', $location_id);
                },
                'client'
            ])
                ->where([
                    ['type_id', '=', $type_id],
                    ['location_id', '=', $location_id],

                ])
                ->orderBy('id', 'desc')->paginate($limit);

            // Append limit
            $employee->appends(array(
                'limit' => $limit
            ));

            // Convert objects into Array
            $employeesArray = $employee->toArray();

            $gross = array();
            $net = array();
            $i = 0;

            foreach( $employeesArray['data'] as $employee) {

                $totalHours = Setting::where('name', '=', 'total_hours')->first();
                $exchangeRate = Setting::where('name', '=', 'api_key')->first();


                if($employee['hours_worked'] == 0){
                    $gross = $employee['rate'] * $totalHours['value'];
                } else {
                    $gross = $employee['rate'] * $employee['hours_worked'] ;
                }

                if($employee['salary'] != 0){
                    $net = $gross - $employee['salary'] / $exchangeRate['value'] ;
                } else {
                    $net = $gross - $employee['gross'];
                    if($net > 0) {
                    } else {
                        $net = 0;
                    }
                }

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'net_' => $net
                    ]);

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'gross_' => $gross
                    ]);

                $i++;
            }


            // Response
            return response()->json([
                'total'         => $employeesArray['total'],
                'per_page'      => $employeesArray['per_page'],
                'current_page'  => $employeesArray['current_page'],
                'last_page'     => $employeesArray['last_page'],
                'next_page_url' => $employeesArray['next_page_url'],
                'prev_page_url' => $employeesArray['prev_page_url'],
                'from'          => $employeesArray['from'],
                'to'            => $employeesArray['to'],
                'data'          => $employeesArray['data']
            ], 200);

        }
        elseif($request->type && $request->client){

            // Get list
            $employee = Employee::with([
                'type'=> function($query) use ($type_id) {
                    $query->where('id', $type_id);
                },
                'location',
                'client'=> function($query) use ($client_id) {
                    $query->where('id', $client_id);
                }
            ])
                ->where([
                    ['type_id', '=', $type_id],
                    ['client_id', '=', $client_id],
                ])
                ->orderBy('id', 'desc')->paginate($limit);

            // Append limit
            $employee->appends(array(
                'limit' => $limit
            ));

            // Convert objects into Array
            $employeesArray = $employee->toArray();

            $gross = array();
            $net = array();
            $i = 0;

            foreach( $employeesArray['data'] as $employee) {

                $totalHours = Setting::where('name', '=', 'total_hours')->first();
                $exchangeRate = Setting::where('name', '=', 'api_key')->first();


                if($employee['hours_worked'] == 0){
                    $gross = $employee['rate'] * $totalHours['value'];
                } else {
                    $gross = $employee['rate'] * $employee['hours_worked'] ;
                }

                if($employee['salary'] != 0){
                    $net = $gross - $employee['salary'] / $exchangeRate['value'] ;
                } else {
                    $net = $gross - $employee['gross'];
                    if($net > 0) {
                    } else {
                        $net = 0;
                    }
                }

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'net_' => $net
                    ]);

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'gross_' => $gross
                    ]);

                $i++;
            }


            // Response
            return response()->json([
                'total'         => $employeesArray['total'],
                'per_page'      => $employeesArray['per_page'],
                'current_page'  => $employeesArray['current_page'],
                'last_page'     => $employeesArray['last_page'],
                'next_page_url' => $employeesArray['next_page_url'],
                'prev_page_url' => $employeesArray['prev_page_url'],
                'from'          => $employeesArray['from'],
                'to'            => $employeesArray['to'],
                'data'          => $employeesArray['data']
            ], 200);

        }
        elseif($request->location && $request->client){
            // Get list
            $employee = Employee::with([
                'type',
                'location'=> function($query) use ($location_id) {
                    $query->where('id', $location_id);
                },
                'client'=> function($query) use ($client_id) {
                    $query->where('id', $client_id);
                }
            ])
                ->where([
                    ['location_id', '=', $location_id],
                    ['client_id', '=', $client_id],
                ])
                ->orderBy('id', 'desc')->paginate($limit);

            // Append limit
            $employee->appends(array(
                'limit' => $limit
            ));

            // Convert objects into Array
            $employeesArray = $employee->toArray();

            $gross = array();
            $net = array();
            $i = 0;

            foreach( $employeesArray['data'] as $employee) {

                $totalHours = Setting::where('name', '=', 'total_hours')->first();
                $exchangeRate = Setting::where('name', '=', 'api_key')->first();


                if($employee['hours_worked'] == 0){
                    $gross = $employee['rate'] * $totalHours['value'];
                } else {
                    $gross = $employee['rate'] * $employee['hours_worked'] ;
                }

                if($employee['salary'] != 0){
                    $net = $gross - $employee['salary'] / $exchangeRate['value'] ;
                } else {
                    $net = $gross - $employee['gross'];
                    if($net > 0) {
                    } else {
                        $net = 0;
                    }
                }

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'net_' => $net
                    ]);

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'gross_' => $gross
                    ]);

                $i++;
            }


            // Response
            return response()->json([
                'total'         => $employeesArray['total'],
                'per_page'      => $employeesArray['per_page'],
                'current_page'  => $employeesArray['current_page'],
                'last_page'     => $employeesArray['last_page'],
                'next_page_url' => $employeesArray['next_page_url'],
                'prev_page_url' => $employeesArray['prev_page_url'],
                'from'          => $employeesArray['from'],
                'to'            => $employeesArray['to'],
                'data'          => $employeesArray['data']
            ], 200);

        }
        elseif($request->type){
            // Get list
            $employee = Employee::with([
                'type'=> function($query) use ($type_id) {
                    $query->where('id', $type_id);
                },
                'location',
                'client'
            ])
                ->where([
                    ['type_id', '=', $type_id],
                ])
                ->orderBy('id', 'desc')->paginate($limit);

            // Append limit
            $employee->appends(array(
                'limit' => $limit
            ));

            // Convert objects into Array
            $employeesArray = $employee->toArray();

            $gross = array();
            $net = array();
            $i = 0;

            foreach( $employeesArray['data'] as $employee) {

                $totalHours = Setting::where('name', '=', 'total_hours')->first();
                $exchangeRate = Setting::where('name', '=', 'api_key')->first();


                if($employee['hours_worked'] == 0){
                    $gross = $employee['rate'] * $totalHours['value'];
                } else {
                    $gross = $employee['rate'] * $employee['hours_worked'] ;
                }

                if($employee['salary'] != 0){
                    $net = $gross - $employee['salary'] / $exchangeRate['value'] ;
                } else {
                    $net = $gross - $employee['gross'];
                    if($net > 0) {
                    } else {
                        $net = 0;
                    }
                }

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'net_' => $net
                    ]);

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'gross_' => $gross
                    ]);

                $i++;
            }


            // Response
            return response()->json([
                'total'         => $employeesArray['total'],
                'per_page'      => $employeesArray['per_page'],
                'current_page'  => $employeesArray['current_page'],
                'last_page'     => $employeesArray['last_page'],
                'next_page_url' => $employeesArray['next_page_url'],
                'prev_page_url' => $employeesArray['prev_page_url'],
                'from'          => $employeesArray['from'],
                'to'            => $employeesArray['to'],
                'data'          => $employeesArray['data']
            ], 200);

        }
        elseif( $request->client){
            // Get list
            $employee = Employee::with([
                'type',
                'location',
                'client'=> function($query) use ($client_id) {
                    $query->where('id', $client_id);
                }
            ])
                ->where([
                    ['client_id', '=', $client_id],
                ])
                ->orderBy('id', 'desc')->paginate($limit);

            // Append limit
            $employee->appends(array(
                'limit' => $limit
            ));

            // Convert objects into Array
            $employeesArray = $employee->toArray();

            $gross = array();
            $net = array();
            $i = 0;

            foreach( $employeesArray['data'] as $employee) {

                $totalHours = Setting::where('name', '=', 'total_hours')->first();
                $exchangeRate = Setting::where('name', '=', 'api_key')->first();


                if($employee['hours_worked'] == 0){
                    $gross = $employee['rate'] * $totalHours['value'];
                } else {
                    $gross = $employee['rate'] * $employee['hours_worked'] ;
                }

                if($employee['salary'] != 0){
                    $net = $gross - $employee['salary'] / $exchangeRate['value'] ;
                } else {
                    $net = $gross - $employee['gross'];
                    if($net > 0) {
                    } else {
                        $net = 0;
                    }
                }

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'net_' => $net
                    ]);

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'gross_' => $gross
                    ]);

                $i++;
            }


            // Response
            return response()->json([
                'total'         => $employeesArray['total'],
                'per_page'      => $employeesArray['per_page'],
                'current_page'  => $employeesArray['current_page'],
                'last_page'     => $employeesArray['last_page'],
                'next_page_url' => $employeesArray['next_page_url'],
                'prev_page_url' => $employeesArray['prev_page_url'],
                'from'          => $employeesArray['from'],
                'to'            => $employeesArray['to'],
                'data'          => $employeesArray['data']
            ], 200);

        }
        elseif($request->location){

            // Get list
            $employee = Employee::with([
                'type',
                'location'=> function($query) use ($location_id) {
                    $query->where('id', $location_id);
                },
                'client'
            ])
                ->where([
                    ['location_id', '=', $location_id],
                ])
                ->orderBy('id', 'desc')->paginate($limit);

            // Append limit
            $employee->appends(array(
                'limit' => $limit
            ));

            // Convert objects into Array
            $employeesArray = $employee->toArray();

            $gross = array();
            $net = array();
            $i = 0;

            foreach( $employeesArray['data'] as $employee) {

                $totalHours = Setting::where('name', '=', 'total_hours')->first();
                $exchangeRate = Setting::where('name', '=', 'api_key')->first();


                if($employee['hours_worked'] == 0){
                    $gross = $employee['rate'] * $totalHours['value'];
                } else {
                    $gross = $employee['rate'] * $employee['hours_worked'] ;
                }

                if($employee['salary'] != 0){
                    $net = $gross - $employee['salary'] / $exchangeRate['value'] ;
                } else {
                    $net = $gross - $employee['gross'];
                    if($net > 0) {
                    } else {
                        $net = 0;
                    }
                }

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'net_' => $net
                    ]);

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'gross_' => $gross
                    ]);

                $i++;
            }


            // Response
            return response()->json([
                'total'         => $employeesArray['total'],
                'per_page'      => $employeesArray['per_page'],
                'current_page'  => $employeesArray['current_page'],
                'last_page'     => $employeesArray['last_page'],
                'next_page_url' => $employeesArray['next_page_url'],
                'prev_page_url' => $employeesArray['prev_page_url'],
                'from'          => $employeesArray['from'],
                'to'            => $employeesArray['to'],
                'data'          => $employeesArray['data']
            ], 200);

        }
        else{
            // Set limit for user input
            $limit = $request->input('limit')?$request->input('limit'):200;

            // Get list of employee
            $department = Department::all();
            $location = Location::all();
            $client = Client::all();

            $employees = Employee::with(['type', 'client', 'location'])->orderBy('id', 'desc')->paginate($limit);

            // Append limit
            $employees->appends(array(
                'limit' => $limit
            ));

            // Convert objects into Array
            $employeesArray = $employees->toArray();

            $gross = array();
            $net = array();
            $i = 0;

            foreach( $employeesArray['data'] as $employee) {

                $totalHours = Setting::where('name', '=', 'total_hours')->first();
                $exchangeRate = Setting::where('name', '=', 'api_key')->first();


                if($employee['hours_worked'] == 0){
                    $gross = $employee['rate'] * $totalHours['value'];
                } else {
                    $gross = $employee['rate'] * $employee['hours_worked'] ;
                }

                if($employee['salary'] != 0){
                    $net = $gross - $employee['salary'] / $exchangeRate['value'] ;
                } else {
                    $net = $gross - $employee['gross'];
                    if($net > 0) {
                    } else {
                        $net = 0;
                    }
                }

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'net_' => $net
                    ]);

                $this->array_insert(
                    $employeesArray['data'][$i],
                    'gender',
                    [
                        'gross_' => $gross
                    ]);

                $i++;
            }


            // Response
            return response()->json([
                'total'         => $employeesArray['total'],
                'per_page'      => $employeesArray['per_page'],
                'current_page'  => $employeesArray['current_page'],
                'last_page'     => $employeesArray['last_page'],
                'next_page_url' => $employeesArray['next_page_url'],
                'prev_page_url' => $employeesArray['prev_page_url'],
                'from'          => $employeesArray['from'],
                'to'            => $employeesArray['to'],
                'data'          => $employeesArray['data']
            ], 200);
        }




    }


}
