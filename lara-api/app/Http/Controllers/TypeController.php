<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class TypeController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        //$this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // Set limit for user input
        $limit = $request->input('limit')?$request->input('limit'):200;

        // Get list of location
        $types = Type::orderBy('name', 'asc')->paginate($limit);

        // Append limit
        $types->appends(array(
            'limit' => $limit
        ));

        // Convert objects into Array
        $clientsArray = $types->toArray();

        // Response
        return response()->json([
            'total'         => $clientsArray['total'],
            'per_page'      => $clientsArray['per_page'],
            'current_page'  => $clientsArray['current_page'],
            'last_page'     => $clientsArray['last_page'],
            'next_page_url' => $clientsArray['next_page_url'],
            'prev_page_url' => $clientsArray['prev_page_url'],
            'from'          => $clientsArray['from'],
            'to'            => $clientsArray['to'],
            'data'          => $clientsArray['data']
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = new Type;
        $type->name = $request->name;
        $type->position = $request->position;
        $type->type = $request->type;
        $type->location_id = $request->location_id;
        $type->save();

        return response()->json(['message' => 'Type added successfully'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get item
        $type = Type::find($id);

        // Response
        return response()->json([
            $type
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Get item
        $type = Type::find($id);

        // Response
        return response()->json([
            $type
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = Type::find($id);

        $type->name = $request->name;
        $type->position = $request->position;

        if($request->location_id == null){
        } else{
            $type->location_id = $request->location_id;
        }
        if($request->type_id == null){
        } else{
            $type->type = $request->type_id;
        }

        $type->save();

        return response()->json(['message' => 'Type edit successfully'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find
        $type = Type::find($id);

        // Delete
        $type->delete();

        return response()->json(['message' => 'Type delete successfully'], 200);
    }
}
