<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class DepartmentController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        //$this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // Set limit for user input
        $limit = $request->input('limit')?$request->input('limit'):200;

        // Get list of location
        $departments = Department::orderBy('id', 'desc')->paginate($limit);

        // Append limit
        $departments->appends(array(
            'limit' => $limit
        ));

        // Convert objects into Array
        $departmentsArray = $departments->toArray();

        // Response
        return response()->json([
            'total'         => $departmentsArray['total'],
            'per_page'      => $departmentsArray['per_page'],
            'current_page'  => $departmentsArray['current_page'],
            'last_page'     => $departmentsArray['last_page'],
            'next_page_url' => $departmentsArray['next_page_url'],
            'prev_page_url' => $departmentsArray['prev_page_url'],
            'from'          => $departmentsArray['from'],
            'to'            => $departmentsArray['to'],
            'data'          => $departmentsArray['data']
        ], 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $department = new Department;
        $department->name = $request->name;
        $department->save();

        return response()->json(['message' => 'Department added successfully'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get item
        $department = Department::find($id);

        // Response
        return response()->json([
            $department
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $department = Department::find($id);

        $department->name = $request->name;

        $department->save();

        return response()->json(['message' => 'Department edit successfully'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find
        $department = Department::find($id);

        // Delete
        $department->delete();

        return response()->json(['message' => 'Location delete successfully'], 200);
    }
}
