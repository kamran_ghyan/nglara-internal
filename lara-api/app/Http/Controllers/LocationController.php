<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Location;
use App\Setting;
use Mockery\Undefined;
use Tymon\JWTAuth\JWTAuth;
use App\Employee;
use Tymon\JWTAuth\Exceptions\JWTException;

class LocationController extends Controller
{
    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        //$this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, JWTAuth $JWTAuth)
    {

        // Set limit for user input
        $limit = $request->input('limit')?$request->input('limit'):200;

        // Get list of location
        $locations = Location::orderBy('id', 'desc')->paginate($limit);

        // Append limit
        $locations->appends(array(
            'limit' => $limit
        ));

        // Convert objects into Array
        $locationsArray = $locations->toArray();

        // Response
        return response()->json([
            'total' => $locationsArray['total'],
            'per_page' => $locationsArray['per_page'],
            'current_page' => $locationsArray['current_page'],
            'last_page' => $locationsArray['last_page'],
            'next_page_url' => $locationsArray['next_page_url'],
            'prev_page_url' => $locationsArray['prev_page_url'],
            'from' => $locationsArray['from'],
            'to' => $locationsArray['to'],
            'data' => $locationsArray['data']
        ], 200);


    }

    public function calculateLocationsGross($location){

        $totalIncome = 0;

        $exchangeRate = Setting::where('name', '=', 'api_key')->first();

        $totalHours = Setting::where('name', '=', 'total_hours')->first();

        $grossBy = DB::table('employees')->where([
            ['location_id', '=', $location],
            ['exclude', '!=', 1],
        ])->get(['rate', 'hours_worked', 'location_id',]);



        foreach ($grossBy as $gross){
            if($gross->hours_worked == 0){
                $totalIncome += $gross->rate * $totalHours['value'];
            }
            if($gross->hours_worked != 0){
                $totalIncome += $gross->rate * $gross->hours_worked;
            }

//            if($customtotalHours[0]->hours_worked == 0){
//                echo $totalIncome[] = $rate[0]->rate * $totalHours['value'];
//            } else {
//                echo $totalIncome[] = $rate[0]->rate * $customtotalHours[0]->hours_worked;
//            }
        }

        return $totalIncome;

    }

    public function dashboard(Request $request)
    {

        // Set limit for user input
        $limit = $request->input('limit')?$request->input('limit'):200;

        // Get list of location
        $locations = Location::orderBy('created_at', 'asc')->paginate($limit);


        $totalIncome = array();
        $totalExpense = array();
        $totalNetProfit = array();
        $income = array();
        $support = array();
        $bench = array();
        $incomeSalary = array();
        $supportSalary = array();
        $benchSalary = array();
        $count = array();
        $totalSum = array();
        $usExp = array();
        $totalIncomeEmploye =  '';
        $totalSupportEmploye =  '';
        $totalBenchEmploye =  '';
        $totalEmployeCompany =  '';
        $totalIncomeEmployee_ = array();
        $i = 0;

        foreach($locations as $location){

            // Get hourly rate of employee
            $rate = DB::table('employees')->where([
                ['location_id', '=', $location->id],
            ])->get(['rate',]);

            $totalEmp = DB::table('employees')->where([
                ['location_id', '=', $location->id],
                ['type__id', '=', 1],
                ['type_id', '!=', 9],
            ])->count();


            // USD cost overhead
            $cost_usd = DB::table('overheads')->where([
                ['location_id', '=', $location->id],
            ])->get(['usd_cost', 'cost', 'location_id',]);

            // Get salary type USD or PKR
            $employeeSalaryType = DB::table('employees')->where([
                ['location_id', '=', $location->id],
            ])->get(['salary', 'gross']);

            // Get total hours
            $totalHours = Setting::where('name', '=', 'total_hours')->first();

            // Get exchange rate
            $exchangeRate = Setting::where('name', '=', 'api_key')->first();

            // Get custom hours of employee
            $customtotalHours = DB::table('employees')->where([
                ['location_id', '=', $location->id],
            ])->get(['hours_worked',]);

            $income[] = Employee::where([
                ['type__id', '=', 1],
                ['type_id', '!=', 9],
                ['exclude', '!=', 1],
                ['location_id', '=', $location->id]
            ])->count();

            $support[] = Employee::where([
                ['type__id', '=', 2],
                ['type_id', '!=', 9],
                ['exclude', '!=', 1],
                ['location_id', '=', $location->id]
            ])->count();

            $bench[] = Employee::where([
                ['type_id', '=', 9],
                ['exclude', '!=', 1],
                ['location_id', '=', $location->id]
            ])->count();

            // Count of overhead
            $count[] = DB::table('overheads')->where([['location_id', '=', $location->id],])->count();

            if(isset($employeeSalaryType[0])){
                if($employeeSalaryType[0]->gross != 0){


                    $employee_s_salary = DB::table('employees')->where([
                        ['location_id', '=', $location->id],
                    ])->get(['salary', 'gross', 'location_id',]);

                    $pkr_cost = 0;
                    $usd_cost = 0;

                    foreach ($employee_s_salary as $overhead){
                        if($overhead->gross == 0){
                            $pkr_cost = DB::table('employees')->where([
                                    ['location_id', '=', $location->id],
                                    ['type__id', '=', 2],
                                    ['type_id', '!=', 9],
                                    ['exclude', '!=', 1],
                                ])->sum('salary') / $exchangeRate['value'];
                        }
                        if($overhead->salary == 0){
                            $usd_cost = DB::table('employees')->where([
                                ['location_id', '=', $location->id],
                                ['type__id', '=', 2],
                                ['type_id', '!=', 9],
                                ['exclude', '!=', 1],
                            ])->sum('gross');

                            $usd_cost = $usd_cost;
                        }
                    }

                    $supportSalary[] = $usd_cost + $pkr_cost;


//                    $income_pkr = 0;
//                    $income_usd = 0;
//
//                    foreach ($employee_s_salary as $overhead){
//                        if($overhead->gross == 0){
//                            $income_pkr = DB::table('employees')->where([
//                                    ['location_id', '=', $location->id],
//                                    ['type__id', '=', 1],
//                                    ['type_id', '!=', 9],
//                                    ['exclude', '!=', 1],
//                                ])->sum('salary');
//                        }
//                        if($overhead->salary == 0){
//                            $income_usd = DB::table('employees')->where([
//                                ['location_id', '=', $location->id],
//                                ['type__id', '=', 1],
//                                ['type_id', '!=', 9],
//                                ['exclude', '!=', 1],
//                            ])->sum('gross') * $exchangeRate['value'];
//                        }
//                    }
//
//                    $incomeSalary[] = $income_pkr + $income_usd;



                    $incomeSalary[] = Employee::where([
                        ['type__id', '=', 1],
                        ['type_id', '!=', 9],
                        ['exclude', '!=', 1],
                        ['location_id', '=', $location->id]
                    ])->sum('gross');

//                    $supportSalary[] = Employee::where([
//                        ['type__id', '=', 2],
//                        ['type_id', '!=', 9],
//                        ['exclude', '!=', 1],
//                        ['location_id', '=', $location->id]
//                    ])->sum('gross');


                    $benchSalary[] = Employee::where([
                        ['type__id', '=', 1],
                        ['type_id', '=', 9],
                        ['exclude', '!=', 1],
                        ['location_id', '=', $location->id]
                    ])->sum('gross');
                }

                if($employeeSalaryType[0]->salary != 0){


                    $incomeSalary[] = Employee::where([
                        ['type__id', '=', 1],
                        ['type_id', '!=', 9],
                        ['exclude', '!=', 1],
                        ['location_id', '=', $location->id]
                    ])->sum('salary');

                    $supportSalary[] = Employee::where([
                        ['type__id', '=', 2],
                        ['type_id', '!=', 9],
                        ['exclude', '!=', 1],
                        ['location_id', '=', $location->id]
                    ])->sum('salary');


                    $benchSalary[] = Employee::where([
                        ['type__id', '=', 1],
                        ['type_id', '=', 9],
                        ['exclude', '!=', 1],
                        ['location_id', '=', $location->id]
                    ])->sum('salary');
                }
            }


            if(
                isset($cost_usd[0]) &&
                isset($employeeSalaryType[0]) &&
                isset($customtotalHours[0])
            ) {

//                if($customtotalHours[0]->hours_worked == 0){
//                    echo $totalIncome[] = $rate[0]->rate * $totalHours['value'];
//                } else {
//                    echo $totalIncome[] = $rate[0]->rate * $customtotalHours[0]->hours_worked;
//                }
//
//                $totalIncomeEmployee_[] = $totalIncome[$i] * $totalEmp;
                $totalIncomeEmployee_[] = $this->calculateLocationsGross($location->id);


                if($cost_usd[0]->usd_cost != 0){
                    $cost_overheads = DB::table('overheads')->where([
                        ['location_id', '=', $location->id],
                    ])->get(['usd_cost', 'cost', 'location_id',]);

                    $pkr_cost = 0;
                    $usd_cost = 0;

                    foreach ($cost_overheads as $overhead){
                        if($overhead->usd_cost == 0){
                            $pkr_cost = DB::table('overheads')->where([
                                    ['location_id', '=', $location->id],
                                ])->sum('cost') / $exchangeRate['value'];
                        }
                        if($overhead->cost == 0){
                            $usd_cost = DB::table('overheads')->where([
                                ['location_id', '=', $location->id],
                            ])->sum('usd_cost');

                            $usd_cost = $usd_cost ;
                        }
                    }

                    $totalSum[] = $usd_cost + $pkr_cost;
                    $usExp[] = 1;

                }
                else {

                    $cost_overheads = DB::table('overheads')->where([
                        ['location_id', '=', $location->id],
                    ])->get(['usd_cost', 'cost', 'location_id',]);

                    $pkr_cost = 0;
                    $usd_cost = 0;

                    foreach ($cost_overheads as $overhead){
                        if($overhead->usd_cost == 0){
                            $pkr_cost = DB::table('overheads')->where([
                                ['location_id', '=', $location->id],
                            ])->sum('cost');
                        }
                        if($overhead->cost == 0){
                            $usd_cost = DB::table('overheads')->where([
                                ['location_id', '=', $location->id],
                            ])->sum('usd_cost');

                            $usd_cost = $usd_cost * $exchangeRate['value'];
                        }
                    }

                    $totalSum[] = $usd_cost + $pkr_cost;
                    $usExp[] = 0;
                }






                if($cost_usd[0]->usd_cost != 0 && trim($location->name, ' ') != 'Norman' && trim($location->name, ' ') != 'norman'){

                    if(isset($totalSum[$i])){
                        $expense = $totalSum[$i];
                    } else {
                        $expense =  0;
                    }
                    $totalExpense[] = $expense + $incomeSalary[$i] / $exchangeRate['value'] + $supportSalary[$i] / $exchangeRate['value'] + $benchSalary[$i] / $exchangeRate['value'];

                } else {
                    if(isset($totalSum[$i])){
                        $expense = $totalSum[$i];
                    } else {
                        $expense =  0;
                    }

                    $totalExpense[] = $expense + $incomeSalary[$i] + $supportSalary[$i] + $benchSalary[$i];


                }

                if(isset($totalIncome[$i])){
                    $tIncome = $totalIncome[$i];
                } else {
                    $tIncome = 0;
                }

                $totalNetProfit[] =  $tIncome - isset($totalExpense[$i]);




            } else{

                if($customtotalHours[0]->hours_worked == 0){
                    $totalIncome[] = $rate[0]->rate * $totalHours['value'];
                } else {
                    $totalIncome[] = $rate[0]->rate * $customtotalHours[0]->hours_worked;
                }

                $totalIncomeEmployee_[] = $totalIncome[$i] * $totalEmp;

                if(trim($location->name, ' ') == 'Norman' || trim($location->name, ' ') == 'norman'){
                    $totalSum[] = DB::table('overheads')->where([
                        ['location_id', '=', $location->id],
                    ])->sum('usd_cost');
                    $usExp[] = 1;
                }

                else {
                    $totalSum[] = DB::table('overheads')->where([
                        ['location_id', '=', $location->id],
                    ])->sum('cost');

                    $usExp[] = 0;
                }


                if(isset($totalSum[$i])){
                    $expense = $totalSum[$i];
                } else {
                    $expense =  0;
                }

                $totalExpense[] = $expense + $incomeSalary[$i] + $supportSalary[$i] + $benchSalary[$i];

                if(isset($totalIncome[$i])){
                    $tIncome = $totalIncome[$i];
                } else {
                    $tIncome = 0;
                }

                $totalNetProfit[] =  $tIncome - isset($totalExpense[$i]);



            }

            $totalIncomeEmploye +=  $income[$i];
            $totalSupportEmploye +=  $support[$i];
            $totalBenchEmploye +=  $bench[$i];
            $totalEmployeCompany +=  $income[$i] +  $support[$i] + $bench[$i];

            $i++;
        }


        // Append limit
        $locations->appends(array(
            'limit' => $limit
        ));

        // Convert objects into Array
        $locationsArray = $locations->toArray();

        // Response
        return response()->json([
            'totalIncomeEmployee'  => $totalIncomeEmploye,
            'totalSupportEmployee' => $totalSupportEmploye,
            'totalBenchEmployee' => $totalBenchEmploye,
            'totalEmployee' => $totalEmployeCompany,
            'totalIncome'   => $totalIncomeEmployee_,
            'totalExpense'  => $totalExpense,
            'totalNetProfit' => $totalNetProfit,
            'count'         => $count,
            'overheadSum'   => $totalSum,
            'usExp'         => $usExp,
            'income'        => $income,
            'support'       => $support,
            'bench'         => $bench,
            'incomeSalary'  => $incomeSalary,
            'supportSalary' => $supportSalary,
            'benchSalary'   => $benchSalary,
            'total'         => $locationsArray['total'],
            'per_page'      => $locationsArray['per_page'],
            'current_page'  => $locationsArray['current_page'],
            'last_page'     => $locationsArray['last_page'],
            'next_page_url' => $locationsArray['next_page_url'],
            'prev_page_url' => $locationsArray['prev_page_url'],
            'from'          => $locationsArray['from'],
            'to'            => $locationsArray['to'],
            'data'          => $locationsArray['data']
        ], 200);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $location = new Location;
        $location->name = $request->name;
        $location->save();

        return response()->json(['message' => 'Location added successfully'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get item
        $location = Location::find($id);

        // Response
        return response()->json([
            $location
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $location = Location::find($id);
        $location->name = $request->name;

        $location->save();

        return response()->json(['message' => 'Location edit successfully'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find location
        $location = Location::find($id);

        // Delete location
        $location->delete();

        return response()->json(['message' => 'Location delete successfully'], 200);
    }
}
