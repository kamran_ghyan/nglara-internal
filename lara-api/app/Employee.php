<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public function department(){
        return $this->belongsTo('App\Department');
    }

    public function type(){
        return $this->belongsTo('App\Type');
    }

    public function client(){
        return $this->belongsTo('App\Client');
    }

    public function location(){
        return $this->belongsTo('App\Location');
    }
}
