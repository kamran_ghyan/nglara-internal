<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

if (Request::is("api/*")) {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Allow-Methods: GET, HEAD, POST, DELETE, PUT, OPTIONS");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type,  Accept, Authorization, If-Modified-Since, Cache-Control, Pragma");
}

// App prefix routes
Route::group(['prefix' => 'api/v1'], function(){
    Route::resource('department', 'DepartmentController');
    Route::get('employee/crm', 'EmployeeController@allCrm');
    Route::get('employee/export', 'EmployeeController@export');
    Route::get('employee/manager', 'EmployeeController@allManager');
    Route::post('employee/import', 'EmployeeController@import');
    Route::post('employee/reset', 'EmployeeController@reset');
    Route::post('employee/alltoggle', 'EmployeeController@allToggle');
    Route::post('employee/search', 'EmployeeController@search');
    Route::put('employee/{id}/salary', 'EmployeeController@updateSalary');
    Route::put('employee/{id}/rate', 'EmployeeController@updateRate');
    Route::put('employee/{id}/exclude', 'EmployeeController@updateExclude');
    Route::put('employee/{id}/toggle', 'EmployeeController@updateToggle');
    Route::get('employee/{id}/type', 'EmployeeController@getType');
    Route::get('employee/{id}/location', 'EmployeeController@location');
    Route::get('employee/locations', 'EmployeeController@locations');
    Route::get('employee/{id}/location/{type}/type', 'EmployeeController@locationType');


    Route::resource('employee', 'EmployeeController');
    Route::get('overhead/{id}/sum', 'OverheadController@getSumByLocation');
    Route::get('overhead/sum', 'OverheadController@getSum');
    Route::post('overhead/search', 'OverheadController@search');
    Route::resource('overhead', 'OverheadController');
    Route::get('location/dashboard', 'LocationController@dashboard');
    Route::resource('location', 'LocationController');
    Route::resource('client', 'ClientController');
    Route::resource('type', 'TypeController');
    Route::resource('overhead', 'OverheadController');
    Route::resource('search', 'SearchController');
    Route::post('search', 'SearchController@search');
    Route::post('authenticate', 'Auth\AuthController@authenticate');
    Route::get('setting/{option}', 'SettingController@getOption');
    Route::put('setting/{id}', 'SettingController@update');
    Auth::routes();
    Route::get('/home', 'EmployeeController@index');
});