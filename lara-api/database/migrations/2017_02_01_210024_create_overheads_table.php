<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOverheadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overheads', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('rent');
            $table->unsignedInteger('worker_comp');
            $table->unsignedInteger('general_liability');
            $table->unsignedInteger('software_services');
            $table->unsignedInteger('wire_fee');
            $table->unsignedInteger('office_budget');
            $table->unsignedInteger('merchant_ac_fee');
            $table->unsignedInteger('condado_server');
            $table->unsignedInteger('utilities');
            $table->unsignedInteger('electricity');
            $table->unsignedInteger('opd');
            $table->unsignedInteger('equipment');
            $table->unsignedInteger('entertainment');
            $table->unsignedInteger('eom');
            $table->unsignedInteger('exchange_rate');
            $table->unsignedInteger('leave_encash');
            $table->unsignedInteger('internet');
            $table->unsignedInteger('maintenance');
            $table->unsignedInteger('misc');
            $table->unsignedInteger('bonuses_ot');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overheads');
    }
}
