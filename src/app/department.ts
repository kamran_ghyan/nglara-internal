export class Department{
    id:number;
    name:string;
    description:string;
    created_at:number;
    updated_at:number;
}