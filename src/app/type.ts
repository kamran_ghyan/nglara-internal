export class Type{
    id:number;
    name:string;
    description:string;
    created_at:number;
    updated_at:number;
}