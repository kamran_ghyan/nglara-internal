// Global imports
import { NgModule }             from '@angular/core';
import { CommonModule }         from '@angular/common';
import { BrowserModule }        from '@angular/platform-browser';
import { FormsModule }          from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule }           from '@angular/http';
import { DataTableModule } from "angular2-datatable";
import { FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';



// Local imports
import { AppComponent }  from './app.component';
import { HeaderComponent }  from './components/layout/header/header.component';
import { SidebarComponent }  from './components/layout/sidebar/sidebar.component';
import { ContentComponent }  from './components/layout/content/content.component';
import { OverviewComponent }  from './components/layout/overview/overview.component';
import { GraphComponent }  from './components/layout/graph/graph.component';
import { LocationComponent }  from './components/location/location.component';
import { AddLocationComponent }  from './components/location/add-location.component';
import { ClientComponent }  from './components/client/client.component';
import { AddClientComponent }  from './components/client/add-client.component';
import { TypeComponent }  from './components/type/type.component';
import { AddTypeComponent }  from './components/type/add-type.component';
import { EditTypeComponent }  from './components/type/edit-type.component';
import { DepartmentComponent }  from './components/department/department.component';
import { AddDepartmentComponent }  from './components/department/add-department.component';
import { OverheadReportComponent }  from './components/overhead/overhead-report.component';
import { OverheadComponent }  from './components/overhead/overhead.component';
import { AddOverheadComponent }  from './components/overhead/add-overhead.component';
import { EditOverheadComponent }  from './components/overhead/edit-overhead.component';
import { EmployeeComponent }  from './components/employee/employee.component';
import { AddEmployeeComponent }  from './components/employee/add-employee.component';
import { EditEmployeeComponent }  from './components/employee/edit-employee.component';
import { ReportsComponent }  from './components/reports/reports.component';
import { ReportsByLocationComponent }  from './components/reports/reportby-location.component';
import { CurrencyApiComponent }  from './components/currency-api/currency-api.component';
import { LoggingHoursComponent }  from './components/logging-hours/logging-hours.component';
import { UsersComponent }  from './components/users/users.component';
import { SettingComponent }  from './components/setting/setting.component';
import { DashboardComponent }  from './components/dashboard/dashboard.component';
import { ExportComponent }  from './components/export/export.component';
import { DataImportComponent }  from './components/export/dataimport.component';
import { ResetComponent }  from './components/export/reset.component';

// Directive Imports
import {ContenteditableModelDirective} from './directives/contenteditableModel.directive'

// Filter Imports
import { DataFilterPipe }  from './components/reports/data-filter.pipe';
import { OrderBy }  from './components/reports/orderby-filter.pipe';
import { DropdownLocationFilter } from './components/reports/location-filter.pipe';
import { DropdownClientFilter } from './components/reports/client-filter.pipe';
import { DropdownDepartmentFilter } from './components/reports/department-filter.pipe';

const appRoutes: Routes = [
  // { path: '', component: ContentComponent },
  { path: '', component: DashboardComponent },
  
  { 
    path: 'location', 
    component: LocationComponent,
  },
  { 
    path: 'location/add', 
    component: AddLocationComponent,
  },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'client', component: ClientComponent },
  { path: 'client/add', component: AddClientComponent },
  { path: 'type', component: TypeComponent },
  { path: 'type/add', component: AddTypeComponent },
  { path: 'type/:id/edit', component: EditTypeComponent },
  { path: 'department', component: DepartmentComponent },
  { path: 'department/add', component: AddDepartmentComponent },
  { path: 'overheadreport', component: OverheadReportComponent },
  { path: 'overhead', component: OverheadComponent },
  { path: 'overhead/add', component: AddOverheadComponent },
  { path: 'overhead/:id/edit', component: EditOverheadComponent },
  { path: 'employee', component: EmployeeComponent },
  { path: 'employee/add', component: AddEmployeeComponent },
  { path: 'employee/:id/edit', component: EditEmployeeComponent },
  { path: 'reports', component: ReportsComponent },
  { path: 'reports/:id/loc/:type/type', component: ReportsComponent },
  { path: 'reports/:id/loc/:overhead/overhead', component: ReportsComponent },
  { path: 'overheadreport/:loc/loc', component: OverheadReportComponent },
  { path: 'reports/:id/loc/:bench/bench', component: ReportsComponent },
  { path: 'exchange-rate', component: CurrencyApiComponent },
  { path: 'total-hours', component: LoggingHoursComponent },
  { path: 'user', component: UsersComponent },
  { path: 'reset',   redirectTo: '/', pathMatch: 'full' },
  { path: 'setting', component: LocationComponent },
  { path: 'export', component: ExportComponent },
  { path: 'import', component: DataImportComponent },
  { path: 'reset-employees', component: ResetComponent },
  
];

@NgModule({
  imports:      [ 
    BrowserModule,
    HttpModule,
    FormsModule,
    DataTableModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [ 
    AppComponent, 
    HeaderComponent, 
    SidebarComponent, 
    ContentComponent, 
    OverviewComponent, 
    GraphComponent,
    LocationComponent,
    AddLocationComponent,
    ClientComponent,
    AddClientComponent,
    TypeComponent,
    AddTypeComponent,
    EditTypeComponent,
    DepartmentComponent,
    AddDepartmentComponent,
     OverheadReportComponent,
    OverheadComponent,
    AddOverheadComponent,
    EditOverheadComponent,
    EmployeeComponent,
    AddEmployeeComponent,
    EditEmployeeComponent,
    ReportsComponent,
    ReportsByLocationComponent,
    CurrencyApiComponent,
    LoggingHoursComponent,
    SettingComponent,
    DropdownLocationFilter,
    DropdownClientFilter,
    DropdownDepartmentFilter,
    DataFilterPipe,
    OrderBy,
    DashboardComponent,
    ContenteditableModelDirective,
    UsersComponent,
    ExportComponent,
    ResetComponent,
    FileSelectDirective,
    DataImportComponent
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
