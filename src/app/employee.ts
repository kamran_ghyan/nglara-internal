export class Employee{
    id:number;
    name:string;
    description:string;
    salary:string;
    gender:string;
    rate:string;
    type:string;
    department_id:number;
    location_id:any;
    type_id:number;
    hours_worked:number;
    created_at:number;
    updated_at:number;
}