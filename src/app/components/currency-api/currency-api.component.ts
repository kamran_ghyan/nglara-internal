// Global Imports
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import 'rxjs/add/operator/map';

// Service Imports
import { SettingService } from '../../services/setting.service';

// Model Imports
import { Setting } from '../../setting';

@Component({
  moduleId: module.id,
  selector: 'currency-api',
  templateUrl: 'currency-api.component.html',
})
export class CurrencyApiComponent  implements OnInit, AfterViewInit{ 

  api_key:any;
  api_secret:any;
  isSaved = false;

  constructor(private _settingService:SettingService){}

  @ViewChild('msgFocous') vc: any;

  ngAfterViewInit() {
    this.vc.nativeElement.focus();
  }

  ngOnInit(){
    // get API key
    this._settingService.get('api_key')
    .subscribe(api_key => {
      this.api_key = api_key[0].value;
    });
  }

  onSubmit(form:any) { 
    var updateTotalHours = {
        api_key:     form.api_key,
        option:          'api_key'
      };
       this._settingService.update(updateTotalHours).subscribe(res => {
        this.isSaved = true;
        setTimeout(function() {
            this.isSaved = false;
        }.bind(this), 3000);
       });
  }

}