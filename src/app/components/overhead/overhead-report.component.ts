// Global Imports
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/map';

// Service Imports
import { OverheadService } from '../../services/overhead.service';
import { LocationService } from '../../services/location.service';
import { SettingService } from '../../services/setting.service';

// Model Imports
import { Overhead } from '../../overhead';

@Component({
  moduleId: module.id,
  selector: 'overhead',
  templateUrl: 'overhead-report.component.html',
})

export class OverheadReportComponent implements OnInit  {

  data: any;
  locations:any;
  isSaved = false;
  noFound = false;
  updatedValue:string;
  excahngeRate:any;
  sumPKR:any;
  sumUSD:any;

  constructor(
      private _overheadService:OverheadService,
      private route: ActivatedRoute,
      private _settingService:SettingService,
      private _locationService:LocationService
      ){}

  ngOnInit(){

    // get API ket
    this._settingService.get('api_key')
    .subscribe(exchangeRate => {
      this.excahngeRate = exchangeRate[0].value;
    });

     
    // get all location
    this.locations = [];
    this._locationService.get()
    .subscribe(locations => {
      this.locations = locations.data;
    });
    
    if(this.route.snapshot.params['loc']){
       var search = {
            location: this.route.snapshot.params['loc']
        };
        
        // get serached employees
        this.data = [];
        this._overheadService.getSearch(search)
        .subscribe(allOverhead => {
        this.data = allOverhead.data;
        console.log(this.data);
        this.sumPKR = this.calculateExpense(allOverhead.data);
        console.log(this.sumPKR);
        this.sumUSD = this.calculateExpense(allOverhead.data);
        if(this.data.length > 0 ){
            this.noFound = false;
        } else{
            this.noFound = true
        }
        })
    } else {

      this.data = [];
      this._overheadService.get()
      .subscribe(overheads => {
        this.data = overheads.data;
        this.sumPKR = this.calculateExpense(overheads.data);
        this.sumUSD = this.calculateExpense(overheads.data);
      });

    }
    
    

    
  }

  // Edit state mode
    setEditState(overhead:any, state:any){
        if(state){
            overhead.isEditMode = state;
        } else {
            delete overhead.isEditMode;
        }
    }

    // Update 
    update(event:any, overhead:any){

        if(event.which === 13){
          var _overhead = {
            id: overhead.id,
            cost: overhead.cost
          };

          this._overheadService.update(overhead)
          .subscribe(data => {
            this.setEditState(overhead, false);
          });
        }
      
    }

    // Update 
    updateUsExpense(event:any, overhead:any){

        if(event.which === 13){
          var _overhead = {
            id: overhead.id,
            usd_cost: overhead.usd_cost
          };

          this._overheadService.update(overhead)
          .subscribe(data => {
            this.setEditState(overhead, false);
          });
        }
      
    }

  // Delete 
    delete(id:any){
      if (confirm("Are you sure you want to delete this Overhead?")) {
        this._overheadService.delete(id)
        .subscribe(data => {

            this.ngOnInit();

            this.isSaved = true;
            setTimeout(function() {
                this.isSaved = false;
            }.bind(this), 3000);

          });
      }
      
    }

     onSubmit(event:any){
        var selectionName = event.target.name;
        var selectionValue = event.target.value;
        var search = {
            location: selectionValue
        };
        
        // get serached employees
        this.data = [];
        this._overheadService.getSearch(search)
        .subscribe(allOverhead => {
        this.data = allOverhead.data;
        this.sumPKR = this.calculateExpense(allOverhead.data);
        this.sumUSD = this.calculateExpense(allOverhead.data);
        if(this.data.length > 0 ){
            this.noFound = false;
        } else{
            this.noFound = true
        }
        });
  }

  calculateExpense(data:any){

    let totalPKR = 0;
    let totalUSD = 0;
    let avgPKR = 0;
    let avgUSD = 0;
  
    for(let i=0; i < data.length; i++){
      if(data[i].usd_cost != 0){
        totalPKR += data[i].usd_cost * this.excahngeRate;
        totalUSD += data[i].usd_cost;
      } else {
        totalPKR += data[i].cost;
        totalUSD += data[i].cost / this.excahngeRate;
      }
           
    }
    
    avgPKR = totalPKR / data.length;
    avgUSD  = totalUSD / data.length;
    
    return {
      expPKR: totalPKR ,
      expUSD: totalUSD,
      avgPKR: avgPKR,
      avgUSD: avgUSD,
    };

 }
  
 }