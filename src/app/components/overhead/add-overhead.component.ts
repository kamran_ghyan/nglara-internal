import { Component, OnInit , ViewChild, AfterViewInit} from '@angular/core';
import 'rxjs/add/operator/map';

// Service Imports
import { LocationService } from '../../services/location.service';
import { DepartmentService } from '../../services/department.service';
import { ClientService } from '../../services/client.service';
import { EmployeeService } from '../../services/employee.service';
import { OverheadService } from '../../services/overhead.service';
import { SettingService } from '../../services/setting.service';

// Model Imports
import { Location } from '../../location';
import { Department } from '../../department';
import { Client } from '../../client';
import { Employee } from '../../employee';
import { Overhead } from '../../overhead';
import { Setting } from '../../setting';
  
@Component({
  moduleId: module.id,
  selector: 'add-overhead',
  templateUrl: 'add-overhead.component.html',
})

export class AddOverheadComponent implements OnInit, AfterViewInit  { 

   data: any;
   isSaved = false;
   loader = false;
   message:any;
   locations:any;
   departments:any;
   clients:any;
   crms:any;
   managers:any;

   exchangeRate:any;
   totalHours:any;

   showCustomType = true;
   showDefaultType = false; 
   

  constructor(
    private _locationService:LocationService, 
    private _departmentService:DepartmentService, 
    private _clientService:ClientService,
    private _overheadService:OverheadService,
    private _settingService:SettingService,
    private _employeeService:EmployeeService
  ){}

    @ViewChild('msgFocous') vc: any;

  ngAfterViewInit() {
    this.vc.nativeElement.focus();
  }
  
    ngOnInit(){

     // get API ket
    this._settingService.get('api_key')
    .subscribe(exchangeRate => {
      this.exchangeRate = exchangeRate[0].value;
    });
    // get API secret
    this._settingService.get('total_hours')
    .subscribe(totalHours => {
      this.totalHours = totalHours[0].value;
    });

    // get all location
    this.locations = [];
    this._locationService.get()
    .subscribe(locations => {
      this.locations = locations.data;
    });
    // get all departments
    this.departments = [];
    this._departmentService.get()
    .subscribe(departments => {
      this.departments = departments.data;
    });

    // get all clients
    this.clients = [];
    this._clientService.get()
    .subscribe(clients => {
      this.clients = clients.data;
    });

    // get all crms
    this.crms = [];
    this._employeeService.getCrm()
    .subscribe(crms => {
      this.crms = crms;
    });

    // get all managers
    this.managers = [];
    this._employeeService.getManager()
    .subscribe(managers => {
      this.managers = managers;
    });

    // get all overheads
    this.data = [];
    this._overheadService.get()
    .subscribe(overheads => {
      this.data = overheads.data;
    });

  }
  
  // Save 
  add(event:any, employeeName:any){

      var newEmployee = {
        name: employeeName.value
      }; 

     
  }

  // Save 
  onSubmit(form:any){
    this.loader = true;
    var data = this.data;
    var duplicateEntry = false;
    
    for(let i=0; i < data.length; i++){
      
      if(data[i].name == form.name &&  data[i].location_id == form.location){
        duplicateEntry = true;
      }
    }

    if(duplicateEntry){
      if(confirm("Are you sure you want to enter duplicate Overhead?")){

        var newEmployee = {}

        if(form.usdsalary == undefined){
         newEmployee = {
            name:           form.name,
            cost:           form.cost,
            location_id:    form.location
          };
        } else {
          newEmployee = {
            name:           form.name,
            usd_cost:           form.usdsalary,
            location_id:    form.location
          };
        }
         


        this._overheadService.save(newEmployee).subscribe(res => {
        this.isSaved = true;
         this.loader = false;
        setTimeout(function() {
           
            this.isSaved = false;
        }.bind(this), 3000);
        });
      }
    } else{
      
      
      var newEmployee = {}

      if(form.usdsalary == undefined){
         newEmployee = {
            name:           form.name,
            cost:           form.cost,
            location_id:    form.location
          };
        } else {
          newEmployee = {
            name:           form.name,
            usd_cost:           form.usdsalary,
            location_id:    form.location
          };
        }

      this._overheadService.save(newEmployee).subscribe(res => {
      this.isSaved = true;
       this.loader = false;
      setTimeout(function() {
         
          this.isSaved = false;
      }.bind(this), 3000);
      });
    }

    // if(form.totalHours == undefined || form.totalHours == ''){
    //   var newTotalHours = this.totalHours;
    // } else {
    //   newTotalHours = form.totalHours;
    // }

    // var newEmployee = {
    //     name:           form.name,
    //     cost:           form.cost,
    //     location_id:    form.location
    //   }; 


    // this._overheadService.save(newEmployee).subscribe(res => {
    // this.isSaved = true;
    // setTimeout(function() {
    //     this.isSaved = false;
    // }.bind(this), 3000);
    // });
  }

  // toggle
  toggle(event:any, expenseType:any){

    let type = expenseType.checked;
    if(type == false){
      this.showCustomType = true;
      this.showDefaultType = false; 
    } else {
      this.showCustomType = false;
      this.showDefaultType = true;
    }
  } 
 }