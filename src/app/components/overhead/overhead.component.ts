// Global Imports
import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';

// Service Imports
import { OverheadService } from '../../services/overhead.service';
import { SettingService } from '../../services/setting.service';

// Model Imports
import { Overhead } from '../../overhead';

@Component({
  moduleId: module.id,
  selector: 'overhead',
  templateUrl: 'overhead.component.html',
})

export class OverheadComponent implements OnInit  {

  data: any;
  isSaved = false;
  updatedValue:string;
  excahngeRate:any;

  constructor(
    private _overheadService:OverheadService,
    private _settingService:SettingService
  ){}

  ngOnInit(){

    // get API ket
    this._settingService.get('api_key')
    .subscribe(exchangeRate => {
      this.excahngeRate = exchangeRate[0].value;
    });

    this.data = [];
    this._overheadService.get()
    .subscribe(overheads => {
      this.data = overheads.data;
    });
  }

  // Edit state mode
    setEditState(overhead:any, state:any, index:any){
        if(state){
          
              overhead.isEditMode = false;
              overhead.isEditMode = state;
            
        } else {
            delete overhead.isEditMode;
        }
    }

    // Update 
    update(event:any, overhead:any, state:any, index:any){

        if(event.which === 13){
          var _overhead = {
            id: overhead.id,
            cost: overhead.cost
          };

          this._overheadService.update(overhead)
          .subscribe(data => {
            this.setEditState(overhead, !state, index);
          });
        }
      
    }

    updateUsExpense(event:any, overhead:any, state:any, index:any){

        if(event.which === 13){
          var _overhead = {
            id: overhead.id,
            usd_cost: overhead.usd_cost
          };

          this._overheadService.update(overhead)
          .subscribe(data => {
            this.setEditState(overhead, !state, index);
          });
        }
      
    }

  // Delete 
    delete(id:any){
      if (confirm("Are you sure you want to delete this Overhead?")) {
        this._overheadService.delete(id)
        .subscribe(data => {

            this.ngOnInit();

            this.isSaved = true;
            setTimeout(function() {
                this.isSaved = false;
            }.bind(this), 3000);

          });
      }
      
    }
  
 }