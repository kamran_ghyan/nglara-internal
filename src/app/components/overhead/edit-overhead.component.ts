import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/map';

// Service Imports
import { LocationService } from '../../services/location.service';
import { DepartmentService } from '../../services/department.service';
import { ClientService } from '../../services/client.service';
import { EmployeeService } from '../../services/employee.service';
import { OverheadService } from '../../services/overhead.service';
import { SettingService } from '../../services/setting.service';

// Model Imports
import { Location } from '../../location';
import { Department } from '../../department';
import { Client } from '../../client';
import { Employee } from '../../employee';
import { Overhead } from '../../overhead';
import { Setting } from '../../setting';
  
@Component({
  moduleId: module.id,
  selector: 'edit-overhead',
  templateUrl: 'edit-overhead.component.html',
})

export class EditOverheadComponent implements OnInit, AfterViewInit  { 

   overhead: any;
   isSaved = false;
   isDeleted = false;
   loader = false;
   message:any;
   locations:any;
   departments:any;
   clients:any;
   crms:any;
   managers:any;

   exchangeRate:any;
   totalHours:any;
   
   sLocation:any;

   showCustomType:boolean;
   showDefaultType:boolean; 

  constructor(
    private _locationService:LocationService, 
    private _departmentService:DepartmentService, 
    private _clientService:ClientService,
    private _overheadService:OverheadService,
    private _settingService:SettingService,
    private route: ActivatedRoute, 
    private router: Router,
    private _employeeService:EmployeeService
  ){}

  @ViewChild('msgFocous') vc: any;

  ngAfterViewInit() {
    this.vc.nativeElement.focus();
  }


    ngOnInit(){
    
    // get API key
    this._settingService.get('api_key')
    .subscribe(exchangeRate => {
      this.exchangeRate = exchangeRate[0].value;
    });
        
    // get employee
    this.overhead = [];
    this._overheadService.getOverhead(this.route.snapshot.params['id'])
    .subscribe(overhead => {
      this.overhead = overhead[0];
      if(this.overhead.usd_cost == 0){
        this.showCustomType = true;
        this.showDefaultType = false;
      } else {
        this.showCustomType = false;
        this.showDefaultType = true;
      }
    });

      // get all location
      this.locations = [];
      this._locationService.get()
      .subscribe(locations => {
        this.locations = locations.data;
      });

  }
  
  

  // Save 
  add(event:any, employeeName:any){

      var newEmployee = {
        name: employeeName.value
      }; 

     
  }

  // Save 
  onSubmit(form:any){

    this.loader = true;

    var newEmployee = {}

      if(form.cost == undefined){
        newEmployee = {
            id:             form.id,
            name:           form.name,
            usd_cost:       form.usd_cost,
            location_id:    form.location
          };
        
        } else {
           newEmployee = {
            id:             form.id,
            name:           form.name,
            cost:           form.cost,
            location_id:    form.location
          };
        }

        console.log(newEmployee);
       this._overheadService.update(newEmployee).subscribe(res => {
        this.isSaved = true;
        this.loader = false;
        setTimeout(function() {
            this.isSaved = false;
        }.bind(this), 3000);
       });

  }

  // toggle
  toggle(event:any, expenseType:any){

    let type = expenseType.checked;
    if(type == false){
      this.showCustomType = true;
      this.showDefaultType = false; 
    } else {
      this.showCustomType = false;
      this.showDefaultType = true;
    }
  } 

  // Delete 
    delete(id:any){
      if (confirm("Are you sure you want to delete this Overhead?")) {
        this._overheadService.delete(id)
        .subscribe(data => {
            this.isDeleted = true;
            setTimeout(function() {
                this.isDeleted = false;
                this.router.navigate(['./overhead']);
            }.bind(this), 3000);

          });
      }
      
    }

 }