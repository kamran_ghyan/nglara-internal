// Global Imports
import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';

// Service Imports
import { TypeService } from '../../services/type.service';

// Model Imports
import { Type } from '../../type';

@Component({
  moduleId: module.id,
  selector: 'type',
  templateUrl: 'type.component.html',
})

export class TypeComponent implements OnInit  {

  data: any;
  isSaved = false;
  updatedValue:string;

  constructor(private _typeService:TypeService){}

  ngOnInit(){
    this.data = [];
    this._typeService.get()
    .subscribe(types => {
      this.data = types.data;
    });
  }

  // Edit state mode
    setEditState(employee:any, state:any){
        if(state){
            employee.isEditMode = state;
        } else {
            delete employee.isEditMode;
        }
    }

    // Update 
    update(employee:any){

        var _employee = {
          id: employee.id,
          name: employee.name
        };

        this._typeService.update(employee)
        .subscribe(data => {
          this.setEditState(employee, false);
        });
      
    }

  // Delete 
    delete(id:any){
      if (confirm("Are you sure you want to delete this Employee Type?")) {
        this._typeService.delete(id)
        .subscribe(data => {

            this.ngOnInit();

            this.isSaved = true;
            setTimeout(function() {
                this.isSaved = false;
            }.bind(this), 3000);

          });
      }
      
    }
  
 }