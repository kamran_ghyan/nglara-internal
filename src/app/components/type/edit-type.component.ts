import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/map';

// Service Imports
import { LocationService } from '../../services/location.service';
import { TypeService } from '../../services/type.service';
import { SettingService } from '../../services/setting.service';


// Model Imports
import { Location } from '../../location';
import { Type } from '../../type';
  
@Component({
  moduleId: module.id,
  selector: 'edit-type',
  templateUrl: 'edit-type.component.html',
})

export class EditTypeComponent implements OnInit, AfterViewInit { 

   locations: any;
   type: any;
   isSaved = false;
   message:any;
   

  constructor(
    private _typeService:TypeService,
    private route: ActivatedRoute, 
    private _settingService:SettingService,
    private _locationService:LocationService
    ){}

    @ViewChild('msgFocous') vc: any;

    ngAfterViewInit() {
      this.vc.nativeElement.focus();
    }

    ngOnInit(){
      
    // get employee
    this.type = [];
    this._typeService.getType(this.route.snapshot.params['id'])
    .subscribe(type => {
      this.type = type[0];
    });

      // get all location
      this.locations = [];
      this._locationService.get()
      .subscribe(locations => {
        this.locations = locations.data;
      });
    }

  // Save
  add(event:any, employeeName:any){

      var newType = {
        name: employeeName.value
      };

      this._typeService.save(newType).subscribe(res => {
        this.isSaved = true;
        setTimeout(function() {
            this.isSaved = false;
        }.bind(this), 3000);
    });
  }

  // Save 
  onSubmit(form:any){

    var newType = {
        id:            this.route.snapshot.params['id'],
        name:          form.name,
        position:      form.position,
        type_id:          form.empType,
        location_id:   form.location
      };
      console.log(newType);
       this._typeService.update(newType).subscribe(res => {
        this.isSaved = true;
        setTimeout(function() {
            this.isSaved = false;
        }.bind(this), 3000);
       });
  }

 }