import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import 'rxjs/add/operator/map';

// Service Imports
import { LocationService } from '../../services/location.service';
import { TypeService } from '../../services/type.service';


// Model Imports
import { Location } from '../../location';
import { Type } from '../../type';
  
@Component({
  moduleId: module.id,
  selector: 'add-type',
  templateUrl: 'add-type.component.html',
})

export class AddTypeComponent implements OnInit, AfterViewInit { 

   locations: any;
   type: any;
   isSaved = false;
   loader = false;
   message:any;
   data:any;
   

  constructor(
    private _typeService:TypeService,
    private _locationService:LocationService
    ){}

    @ViewChild('msgFocous') vc: any;

  ngAfterViewInit() {
    this.vc.nativeElement.focus();
  }

    ngOnInit(){
      // get all location
      this.locations = [];
      this._locationService.get()
      .subscribe(locations => {
        this.locations = locations.data;
      });

       this.data = [];
      this._typeService.get()
      .subscribe(types => {
        this.data = types.data;
      });
    }

  // Save
  add(event:any, employeeName:any){

      var newType = {
        name: employeeName.value
      };

      this._typeService.save(newType).subscribe(res => {
        this.isSaved = true;
        setTimeout(function() {
            this.isSaved = false;
        }.bind(this), 3000);
    });
  }

  // Save 
  onSubmit(form:any){

    this.loader = true;
    var data = this.data;
    var duplicateEntry = false;
    
    for(let i=0; i < data.length; i++){
      
      if(data[i].name == form.name){
        duplicateEntry = true;
      }
    }

    if(duplicateEntry){
        if(confirm("Are you sure you want to enter duplicate EMployee Type?")){
          var newType = {
          name:           form.name,
          position:       form.position,
          type:           form.type,
          location_id:    form.location
        };

        this._typeService.save(newType).subscribe(res => {
          
        this.isSaved = true;
        this.loader = false;
        setTimeout(function() {
            this.isSaved = false;
        }.bind(this), 3000);
        });
      }
    } else{
        var newType = {
          name:           form.name,
          position:       form.position,
          type:           form.type,
          location_id:    form.location
        };

        this._typeService.save(newType).subscribe(res => {
          
        this.isSaved = true;
        this.loader = false;
        setTimeout(function() {
            this.isSaved = false;
        }.bind(this), 3000);
        });
    }

    
  }

 }