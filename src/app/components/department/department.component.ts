// Global Imports
import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';

// Service Imports
import { DepartmentService } from '../../services/department.service';

// Model Imports
import { Department } from '../../department';

@Component({
  moduleId: module.id,
  selector: 'department',
  templateUrl: 'department.component.html',
})

export class DepartmentComponent implements OnInit  {

  departments: any;
  isSaved = false;
  updatedValue:string;

  constructor(private _departmentService:DepartmentService){}

  ngOnInit(){
    this.departments = [];
    this._departmentService.get()
    .subscribe(departments => {
      this.departments = departments;
    });
  }

  // Edit state mode
    setEditState(department:any, state:any){
        if(state){
            department.isEditMode = state;
        } else {
            delete department.isEditMode;
        }
    }

    // Update location
    update(department:any){

        var _department = {
          id: department.id,
          name: department.name
        };

        this._departmentService.update(department)
        .subscribe(data => {
          this.setEditState(department, false);
        });
      
    }

  // Delete location
    delete(id:any){
      if (confirm("Are you sure you want to delete this Department?")) {
        this._departmentService.delete(id)
        .subscribe(data => {

            this.ngOnInit();
            
            this.isSaved = true;
            
            setTimeout(function() {
                this.isSaved = false;
            }.bind(this), 3000);

          });
      }
      
    }
  
 }