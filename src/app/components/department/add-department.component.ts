import { Component } from '@angular/core';

// Service Imports
import { DepartmentService } from '../../services/department.service';

@Component({
  moduleId: module.id,
  selector: 'add-department',
  templateUrl: 'add-department.component.html',
})
export class AddDepartmentComponent  {

  department: any;
   isSaved = false;
   loader = false;
   message:any;

  constructor(private _departmentService:DepartmentService){}
  // Save todo
  add(event:any, departmentName:any){
      this.loader = true;
      var newDepartment = {
        name: departmentName.value
      };

      this._departmentService.save(newDepartment).subscribe(res => {
        this.isSaved = true;
        this.loader = false;
        setTimeout(function() {
            this.isSaved = false;
        }.bind(this), 3000);
    });
  }
 }