import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
  selector: 'sidebar',
  templateUrl: 'sidebar.component.html',
})
export class SidebarComponent  { 
  
  // Deafult sidebar display none
  loc_display: 'none';
  dep_display: 'none';
  emp_display: 'none';
  clt_display: 'none';

  constructor(){}

  // toggle drop down menu setting
  setStyle(option:string){
    if(option == 'loc'){
      if(this.loc_display){
        return 'block';
      } else {
        return 'none';
      }
    }

    if(option == 'dep'){
      if(this.dep_display){
        
      return 'block';
      } else {
        return 'none';
      }
    }

    if(option == 'emp'){
      if(this.emp_display){
      return 'block';
      } else {
        return 'none';
      }
    }

    if(option == 'clt'){
      if(this.clt_display){
      return 'block';
      } else {
        return 'none';
      }
    }
  }


 }