import { Component } from '@angular/core';
import 'rxjs/add/operator/map';

// Service Imports
import { EmployeeService } from '../../../services/employee.service';
import { ClientService } from '../../../services/client.service';

// Model Imports
import { Employee } from '../../../employee';
import { Client } from '../../../client';

@Component({
    moduleId: module.id,
  selector: 'overview',
  templateUrl: 'overview.component.html',
})
export class OverviewComponent  { 

  totalEmployee: any;
  hoursWorked:any;
  totalHoursWorked = 0;
  totalMale:any[];
  totalFemale:any[];
  gross:any;
  totalGross = 0;
  totalClients:any;

  constructor(private _employeeService:EmployeeService, private _clientService:ClientService){}

  ngOnInit(){

    // Get total employee
    this._employeeService.get()
    .subscribe((employee:any) => {

      // get total employee
      this.totalEmployee = employee.total;
      
      // get average working hours
      this.hoursWorked = [];

      this.hoursWorked = employee.data;
      
      for(let i=0; i < this.hoursWorked.length; i++){
           this.totalHoursWorked = this.totalHoursWorked + this.hoursWorked[i].hours_worked;
      }

      // get total male
      this.totalMale = employee.data.filter((emp: any) => emp.gender == 'male').length;

      // get total female
      this.totalFemale = employee.data.filter((emp: any) => emp.gender == 'female').length;

      // get total gross
      this.gross = [];

      this.gross = employee.data;
      
      for(let i=0; i < this.gross.length; i++){
           this.totalGross = this.totalGross + parseInt(this.gross[i].gross);
      }

    });

    // Get total employee
    this._clientService.get()
    .subscribe(client => {
      this.totalClients = client.total;
    });
  }

}