import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import 'rxjs/add/operator/map';

// Service Imports
import { LocationService } from '../../services/location.service';

@Component({
    moduleId: module.id,
  selector: 'add-location',
  templateUrl: 'add-location.component.html',
})
export class AddLocationComponent implements OnInit, AfterViewInit  { 

   location: any;
   isSaved = false;
   loader = false;
   message:any;
   data:any;

  constructor(private _locationService:LocationService){}

  @ViewChild('msgFocous') vc: any;

  ngAfterViewInit() {
    this.vc.nativeElement.focus();
  }

   ngOnInit(){
    this.data = [];
    this._locationService.get()
    .subscribe(locations => {
      this.data = locations.data;
    });
  }


  // Save todo
  add(event:any, locationName:any){

    this.loader = true;
    var data = this.data;
    var duplicateEntry = false;
    
    for(let i=0; i < data.length; i++){
      
      if(data[i].name == locationName.value){
        duplicateEntry = true;
      }
    }

    if(duplicateEntry){
        if(confirm("Are you sure you want to enter duplicate Location?")){

          var newLocation = {
            name: locationName.value
          };
          
            this._locationService.save(newLocation).subscribe(res => {
            this.isSaved = true;
            this.loader = false;
            setTimeout(function() {
              this.isSaved = false;
            }.bind(this), 3000);
          });
          
      }
    } else{
        var newLocation = {
          name: locationName.value
        };
        
          this._locationService.save(newLocation).subscribe(res => {
          this.isSaved = true;
          this.loader = false;
          setTimeout(function() {
            this.isSaved = false;
          }.bind(this), 3000);
        });
    }

      
  }
 }