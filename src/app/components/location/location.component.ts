// Global Imports
import { Component, OnInit,  } from '@angular/core';
import 'rxjs/add/operator/map';


// Service Imports
import { LocationService } from '../../services/location.service';

// Model Imports
import { Location } from '../../location';

@Component({
  moduleId: module.id,
  selector: 'location',
  templateUrl: 'location.component.html',
})

export class LocationComponent implements OnInit  {

  data: any;
  isSaved = false;
  updatedValue:string;

  constructor(private _locationService:LocationService){}

  ngOnInit(){
    this.data = [];
    this._locationService.get()
    .subscribe(locations => {
      this.data = locations.data;
    });
  }

  // Edit state mode
    setEditState(location:any, state:any, index:any){
     
        if(state){

              location.isEditMode = false;
              location.isEditMode = state;
            
        } else {
            delete location.isEditMode;
        }
    }

    // Update location
    updateLocation(event:any, location:any, status:any, index:any){
        if(event.which === 13){
          var _location = {
            id: location.id,
            name: location.name
          };

          this._locationService.update(_location)
          .subscribe(data => {
            this.setEditState(location, !status, index);
          });
        } 
        if(event.which === 27){
          this.setEditState(location, status, index);
        }
      
    }

  // Delete location
    deleteLocation(id:any){
      if (confirm("Are you sure you want to delete this Location?")) {
        this._locationService.delete(id)
        .subscribe(data => {

            this.ngOnInit();

            this.isSaved = true;
            setTimeout(function() {
                this.isSaved = false;
            }.bind(this), 3000);

          });
      }
      
    }
  
 }