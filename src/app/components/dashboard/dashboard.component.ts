// Global Imports
import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';

// Service Imports
import { LocationService } from '../../services/location.service';
import { DepartmentService } from '../../services/department.service';
import { ClientService } from '../../services/client.service';
import { EmployeeService } from '../../services/employee.service';
import { SettingService } from '../../services/setting.service';
import { OverheadService } from '../../services/overhead.service';


// Model Imports
import { Location } from '../../location';
import { Department } from '../../department';
import { Client } from '../../client';
import { Employee } from '../../employee';
import { Setting } from '../../setting';

@Component({
  moduleId: module.id,
  selector: 'dashboard',
  templateUrl: 'dashboard.component.html',
})
export class DashboardComponent implements OnInit{

  locations:any;
  incomeEmployee:any;
  supportEmployee:any;
  benchEmployee:any;
  incomeEmployeeSalary:any;
  supportEmployeeSalary:any;
  benchEmployeeSalary:any;
  totalOverhead:any;
  totalOverheadExp:any;
  usExpense:any;
  totalIncomeLocation:any;
  totalExpenseLocation:any;
  totalNetProfit:any;
  totalEmployeeIncome:any;
  totalEmployeeSupport:any;
  totalEmployeeBench:any;
  totalEmployee:any;
  allLocationIncome:any;
  allLocationSalaries:any;
  allLocationExpense:any;

  departments:any;
  clients:any;
  crms:any;
  managers:any;
  public data:any;
  sumSalary:any;
  sumGross:any;
  sumNet:any;
  avgSalary:any;
  avgGross:any;
  avgNet:any;
  excahngeRate:any;
  totalHours:any;
  totalSum:any;
  totalSumUsd:any
  

  employeeData:any
  employeeSumSalary:any

  // Dashboard Variables
  totalIncomeEmployee:any;
  totalSupportEmployee:any;

  // get total profit by location islamabad
  islamabadEmpData:any;
  islamabadSumSalary:any;
  totalOverheadIslamabadExp:any
  totalOverheadIslamabad:any;

  // get total profit by location lahore
  lahoreEmpData:any;
  lahoreSumSalary:any;
  totalOverheadLahoreExp:any
  totalOverheadLahore:any;

  // get total profit by location karachi
  karachiEmpData:any;
  karachiSumSalary:any;
  totalOverheadKarachiExp:any
  totalOverheadKarachi:any;

  // get total profit by location norman
  normanEmpData:any;
  normanSumSalary:any;
  totalOverheadNormanExp:any
  totalOverheadNorman:any;
  indication:Boolean;

  public filterQuery = "";
  public rowsOnPage = 10;
  public sortBy = "name";
  public sortOrder = "desc";

  constructor(
    private _locationService:LocationService, 
    private _departmentService:DepartmentService, 
    private _clientService:ClientService,
    private _settingService:SettingService,
    private _overheadService:OverheadService,
    private _employeeService:EmployeeService
  ){}

  

  ngOnInit(): void{

    // get SUM overhead
    this._overheadService.getSum()
    .subscribe(totalSum => {
      this.totalSum = parseInt(totalSum.total);
      this.totalSumUsd = parseInt(totalSum.total_usd);
    });

    // get API ket
    this._settingService.get('api_key')
    .subscribe(exchangeRate => {
      this.excahngeRate = exchangeRate[0].value;
    });
    // get API secret
    this._settingService.get('total_hours')
    .subscribe(totalHours => {
      this.totalHours = totalHours[0].value;
    });

    // get all location
    this.locations = [];
    this._locationService.dashboard()
    .subscribe(locations => {
      this.locations = locations.data;
      this.incomeEmployee = locations.income;
      this.supportEmployee = locations.support;
      this.benchEmployee = locations.bench;
      this.incomeEmployeeSalary = locations.incomeSalary;
      this.supportEmployeeSalary = locations.supportSalary;
      this.benchEmployeeSalary = locations.benchSalary;
      this.totalOverhead = locations.count;
      this.totalOverheadExp = locations.overheadSum;
      this.usExpense = locations.usExp;
      this.totalIncomeLocation = locations.totalIncome;
      this.totalExpenseLocation = locations.totalExpense;
      this.totalNetProfit = locations.totalNetProfit;
      this.totalEmployeeIncome = locations.totalIncomeEmployee;
      this.totalEmployeeSupport = locations.totalSupportEmployee;
      this.totalEmployeeBench = locations.totalBenchEmployee;
      this.totalEmployee = locations.totalEmployee;
      this.allLocationIncome = this.allLocationIncome_(locations.totalIncome);
      this.allLocationSalaries = this.allLocationSalaries_(locations.incomeSalary, locations.supportSalary, locations.benchSalary, locations.usExp);
      this.allLocationExpense = this.allLocationExpense_(locations.overheadSum, locations.usExp);

    });

    
    // get all departments
    this.departments = [];
    this._departmentService.get()
    .subscribe(departments => {
      this.departments = departments.data;
    });



    // get all clients
    this.clients = [];
    this._clientService.get()
    .subscribe(clients => {
      this.clients = clients.data;
    });

    // get all crms
    this.crms = [];
    this._employeeService.getCrm()
    .subscribe(crms => {
      this.crms = crms;
    });

    // get all managers
    this.managers = [];
    this._employeeService.getManager()
    .subscribe(managers => {
      this.managers = managers;
    });

    // get all employee
    //this.allEmployee = [];
    this._employeeService.get()
    .subscribe((allEmployee:any) => {
      this.data = allEmployee.data;
      this.sumSalary = this.totalCalculateSalary(allEmployee.data);
      this.sumGross = this.totalCalculateSalary(allEmployee.data);
      this.sumNet = this.totalCalculateSalary(allEmployee.data); 
      
    });

    // Get Income Employee
    this._employeeService.getType(1)
    .subscribe((totalType:any) => {
      this.totalIncomeEmployee = totalType;
    });

    // Get Support Employee
    this._employeeService.getType(2)
    .subscribe((totalType:any) => {
      this.totalSupportEmployee = totalType;
    });

    // Get total earning by location islamabad
    this._employeeService.getEmpByLocation(2)
    .subscribe((byLocation:any) => {
      this.islamabadEmpData = byLocation;
      this.islamabadSumSalary = this.calculateSalary(byLocation.data);      
    });

    // Get Overhead Expense islamabad
    this._employeeService.getSumByLocation(2)
    .subscribe(totalSum => {
      this.totalOverheadIslamabad = totalSum.count;
      this.totalOverheadIslamabadExp = parseInt(totalSum.total) / this.excahngeRate;
    });

    // Get total earning by location lahore
    this._employeeService.getEmpByLocation(3)
    .subscribe((byLocation:any) => {
      this.lahoreEmpData = byLocation;
      this.lahoreSumSalary = this.calculateSalary(byLocation.data);      
    });

    // Get Overhead Expense lahore
    this._employeeService.getSumByLocation(3)
    .subscribe(totalSum => {
      this.totalOverheadLahore = totalSum.count;
      this.totalOverheadLahoreExp = parseInt(totalSum.total) / this.excahngeRate;
    });

    // Get total earning by location karachi
    this._employeeService.getEmpByLocation(4)
    .subscribe((byLocation:any) => {
      this.karachiEmpData = byLocation;
      this.karachiSumSalary = this.calculateSalary(byLocation.data);
      
    });

    // Get Overhead Expense karachi
    this._employeeService.getSumByLocation(4)
    .subscribe(totalSum => {
      this.totalOverheadKarachi = totalSum.count;
      this.totalOverheadKarachiExp = parseInt(totalSum.total) / this.excahngeRate;
    });

    // Get total earning by location norman
    this._employeeService.getEmpByLocation(1)
    .subscribe((byLocation:any) => {
      this.normanEmpData = byLocation;
      this.normanSumSalary = this.calculateSalary(byLocation.data);
      
    });

    // Get Overhead Expense norman
    this._employeeService.getSumByLocation(1)
    .subscribe(totalSum => {
      this.totalOverheadNorman = totalSum.count;
      this.totalOverheadNormanExp = parseInt(totalSum.total);
    });

  } 

  // Edit state mode
    setEditState(employee:any, state:any){
        if(state == false){
            employee.isEditMode = false;
        } else {
            employee.isEditMode = true;
        }
    }

    // Update location
    updateSalary(event:any, location:any){
      if(event.which === 13){
        var _location = {
          id: location.id,
          salary: location.salary
        };

        this._employeeService.updateSalary(_location)
        .subscribe(data => {
          this.setEditState(location, false);
        });
      }
      
    }
  
  
  totalIncome(){
     this._employeeService.getSumByLocation(1)
    .subscribe(totalSum => {
      this.totalOverheadNorman = totalSum.count;
      this.totalOverheadNormanExp = parseInt(totalSum.total);
    });
  }



  onSubmit(event:any){

    if(event.type == 'click'){
      localStorage.setItem('location', '');
      localStorage.setItem('department', '');
      localStorage.setItem('client', '');

    } else {
        var selectionName = event.target.name;
        var selectionValue = event.target.value;
        
        if(selectionName == 'location'){

          localStorage.setItem('location', '');

          if(selectionValue == '' || selectionValue == undefined){
            localStorage.setItem(selectionName, '');
          } else {
            localStorage.setItem(selectionName, selectionValue);
          }
          
        }
        if(selectionName == 'department'){

          localStorage.setItem('department', '');

        if(selectionValue == '' || selectionValue == undefined){
            localStorage.setItem(selectionName, '');
          } else {
            localStorage.setItem(selectionName, selectionValue);
          }
        }
        if(selectionName == 'client'){

          localStorage.setItem('client', '');

          if(selectionValue == '' || selectionValue == undefined){
            localStorage.setItem(selectionName, '');
          } else {
            localStorage.setItem(selectionName, selectionValue);
          }
        }
    }
    
    

     var search = {
        location: localStorage.getItem('location'),
        department: localStorage.getItem('department'),
        client: localStorage.getItem('client')
      };
    
    // get serached employees
    this.data = [];
    this._employeeService.getSearch(search)
    .subscribe(allEmployee => {
      this.data = allEmployee.data;
      this.sumSalary = this.calculateSalary(allEmployee.data);
      this.sumGross = this.calculateSalary(allEmployee.data);
      this.sumNet = this.calculateSalary(allEmployee.data);
      
    });
  }

   indicationLoss(a:any, b:any, ){
    let totalProfit = a - b;
      if(totalProfit > 0){
        this.indication = true;
        return this.indication;
      } else{
        this.indication = false;
        return this.indication;
      }
  }

  // indicationLoss_(a:any, b:any){
  //   let totalProfit = a - b;
  //     if(totalProfit > 0){
  //       this.indication = true;
  //       return this.indication;
  //     } else{
  //       this.indication = false;
  //       return this.indication;
  //     }
  // }

    indicationLoss_(a:any, b:any, c:any){
    let totalProfit = a - (b + c);
      if(totalProfit > 0){
        this.indication = true;
        return this.indication;
      } else{
        this.indication = false;
        return this.indication;
      }
  }

  totalProfitLoss(a:any, b:any, c:any){
      return a - (b + c);
  }

  calculateSalary(data:any) {
    let totalSalary = 0;
    let totalGross = 0;
    let totalNet = 0;
    let avgSalary = 0;
    let avgGross = 0;
    let avgNet = 0;


    for(let i=0; i < data.length; i++){
           if(data[i].exclude == 1){
           } else {
           if(data[i].hours_worked == 0){
             if(data[i].salary == 0 || data[i].salary == null){
                  totalSalary += data[i].gross;
                  totalGross += 0;
                  totalNet += 0;
               } else {
                  totalSalary += data[i].salary / this.excahngeRate;
                  totalGross += data[i].rate * this.totalHours;
                  totalNet += data[i].rate * this.totalHours - data[i].salary / this.excahngeRate;
                  
               }
              //  totalSalary += data[i].salary / this.excahngeRate;
              // totalGross += data[i].rate * this.totalHours;
              // totalNet += data[i].rate * this.totalHours - data[i].salary / this.excahngeRate;
             } else {
               if(data[i].salary == 0 || data[i].salary == null){
                totalSalary += data[i].gross;
                totalGross += 0;
                totalNet += 0;
               } else {
                totalSalary += data[i].salary / this.excahngeRate;
                totalGross += data[i].rate * data[i].hours_worked;
                totalNet += data[i].rate * data[i].hours_worked - data[i].salary / this.excahngeRate;
               }
              // totalSalary += data[i].salary / this.excahngeRate;
              // totalGross += data[i].rate * data[i].hours_worked;
              // totalNet += data[i].rate * data[i].hours_worked - data[i].salary / this.excahngeRate;
             }
           }
    }
    
    avgSalary = totalSalary/data.length;
    avgGross  = totalGross/data.length;
    avgNet    = totalNet/data.length;
    
    return {
      salary: totalSalary ,
      gross: totalGross,
      net: totalNet,
      avgsalary: avgSalary,
      avggross: avgGross,
      avgnet: avgNet
    };
  }


  allLocationIncome_(data:any){
      let total = 0;
      for(let i=0; i < data.length; i++){
        total += data[i];
      }
      return total;
  }

  allLocationSalaries_(income:any, support:any, bench:any, usType:any){
      let total = 0;
      let incomeSalary = 0;
      let supportSalary = 0;
      let benchSalary = 0;

      for(let i=0; i < income.length; i++){
        incomeSalary += income[i] /  this.excahngeRate;
      }

      for(let i=0; i < support.length; i++){
        
        if(usType[i] == 1){
          supportSalary += parseInt(support[i]);
        } else {
          supportSalary += parseInt(support[i]) /  this.excahngeRate;
        }
      
      }
   

      for(let i=0; i < bench.length; i++){
        benchSalary += bench[i] /  this.excahngeRate;
      }

      total = incomeSalary + supportSalary + benchSalary;
      return total;
  }

  allLocationExpense_(data:any, usType:any){
      let total = 0;
      for(let i=0; i < data.length; i++){

        if(usType[i] == 1){
          total += parseInt(data[i]);
        } else {
          total += parseInt(data[i]) /  this.excahngeRate;
        }

      }
      return total;
  }

   totalCalculateSalary(data:any) {
    let totalSalary = 0;
    let totalGross = 0;
    let totalNet = 0;
    let avgSalary = 0;
    let avgGross = 0;
    let avgNet = 0;


    for(let i=0; i < data.length; i++){
           if(data[i].exclude == 1){
           } else {
           if(data[i].hours_worked == 0){
             if(data[i].salary == 0 || data[i].salary == null){
                  totalSalary += data[i].gross;
                  totalGross += data[i].gross_;
                  totalNet += 0;
               } else {
                  totalSalary += data[i].salary / this.excahngeRate;
                  totalGross += data[i].rate * this.totalHours;
                  totalNet += data[i].rate * this.totalHours - data[i].salary / this.excahngeRate;
                  
               }
              //  totalSalary += data[i].salary / this.excahngeRate;
              // totalGross += data[i].rate * this.totalHours;
              // totalNet += data[i].rate * this.totalHours - data[i].salary / this.excahngeRate;
             } else {
               if(data[i].salary == 0 || data[i].salary == null){
                totalSalary += data[i].gross;
                totalGross += data[i].gross_;
                totalNet += 0;
               } else {
                totalSalary += data[i].salary / this.excahngeRate;
                totalGross += data[i].rate * data[i].hours_worked;
                totalNet += data[i].rate * data[i].hours_worked - data[i].salary / this.excahngeRate;
               }
              // totalSalary += data[i].salary / this.excahngeRate;
              // totalGross += data[i].rate * data[i].hours_worked;
              // totalNet += data[i].rate * data[i].hours_worked - data[i].salary / this.excahngeRate;
             }
           }
    }
    
    avgSalary = totalSalary/data.length;
    avgGross  = totalGross/data.length;
    avgNet    = totalNet/data.length;
    
    return {
      salary: totalSalary ,
      gross: totalGross,
      net: totalNet,
      avgsalary: avgSalary,
      avggross: avgGross,
      avgnet: avgNet
    };
  }

 }