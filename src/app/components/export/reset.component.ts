// Global Imports
import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';

// Local Imports
import { EmployeeService } from '../../services/employee.service';

// Model Imports
import { Setting } from '../../setting';

@Component({
  moduleId: module.id,
  selector: 'currency-api',
  templateUrl: 'reset.component.html',
})
export class ResetComponent  implements OnInit{ 

    message:string;
    isSaved = false;
    loader = false;

    filesToUpload: Array<File>;

  constructor(private _employeeService:EmployeeService){
    this.filesToUpload = [];
  }
 
  ngOnInit(){}

  onSubmit(form:any) { 
    if (confirm("Are you sure you want to reset all data?")) {
        this._employeeService.employeeReset()
        .subscribe((data: any) => {
            this.isSaved = true;
            this.loader = false;
            setTimeout(function() {
                this.isSaved = false;
            }.bind(this), 3000);
            this.message = data.message;
        });
        }
  }

 

}