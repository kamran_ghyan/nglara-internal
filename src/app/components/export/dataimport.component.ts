// Global Imports
import { Component, Input, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';
import {FileUploader, Headers} from 'ng2-file-upload/ng2-file-upload';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload/ng2-file-upload';

// Local Imports
import { EmployeeService } from '../../services/employee.service';

const URL = 'https://psm.allshoreresources.com/api/v1/employee/import';


@Component({
  moduleId: module.id,
  selector: 'currency-api',
  templateUrl: 'dataimport.component.html',
})
export class DataImportComponent  implements OnInit{ 

  public myHeaders: Headers[] = [];
  message:any;
  isSaved = false;
  loader = false;

  filesToUpload: Array<File>;

  constructor(private _employeeService:EmployeeService){
    this.filesToUpload = [];
  }

 
  ngOnInit(){
   
  }

  
  onSubmit(event:any, form:any) {
    console.log(event); 
    //console.log(form);
    // this._employeeService.employeeExport()
    //   .subscribe((data: any) => {
    //     this.isSaved = true;
    //     this.loader = false;
    //     setTimeout(function() {
    //         this.isSaved = false;
    //     }.bind(this), 3000);
    //     this.message = data.message;
    //   });
  }


   upload() {
        this.makeFileRequest(URL, [], this.filesToUpload).then((result) => {
            this.isSaved = true;
            this.message = result;
            setTimeout(function() {
            this.isSaved = false;
            }.bind(this), 3000);
        }, (error) => {
            console.error(error);
        });
    }
 
    fileChangeEvent(fileInput: any){
        this.filesToUpload = <Array<File>> fileInput.target.files;
    }
 
    makeFileRequest(url: string, params: Array<string>, files: Array<File>) {
        return new Promise((resolve, reject) => {
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();
            for(var i = 0; i < files.length; i++) {
                formData.append("file", files[i], files[i].name);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", url, true);
            xhr.send(formData);
        });
    }

}