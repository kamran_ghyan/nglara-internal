// Global Imports
import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';

// Service Imports
import { ClientService } from '../../services/client.service';

// Model Imports
import { Client } from '../../client';

@Component({
  moduleId: module.id,
  selector: 'client',
  templateUrl: 'client.component.html',
})

export class ClientComponent implements OnInit  {

  data: any;
  isSaved = false;
  updatedValue:string;

  constructor(private _clientService:ClientService){}

  ngOnInit(){
    this.data = [];
    this._clientService.get()
    .subscribe(clients => {
      this.data = clients.data;
    });
  }

  // Edit state mode
    setEditState(client:any, state:any, index:any){
        if(state){
           
              client.isEditMode = false;
              client.isEditMode = state;
            
        } else {
            delete client.isEditMode;
        }
    }

    // Update 
    update(event:any, client:any, state:any, index:any){
      if(event.which === 13){
        var _client = {
          id: client.id,
          name: client.name
        };
        
        this._clientService.update(_client)
        .subscribe(data => {
          this.setEditState(client, !state, index);
        });
        }
      
    }

  // Delete 
    delete(id:any){
      if (confirm("Are you sure you want to delete this Client?")) {

        this._clientService.delete(id)
        
        .subscribe(data => {

            this.ngOnInit();

            this.isSaved = true;

            setTimeout(function() {
                this.isSaved = false;
            }.bind(this), 3000);

          });
      }
      
    }
  
 }