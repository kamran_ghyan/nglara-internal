// Global Imports
import { Component, ViewChild, AfterViewInit } from '@angular/core';
import 'rxjs/add/operator/map';

// Service Imports
import { ClientService } from '../../services/client.service';

@Component({
    moduleId: module.id,
  selector: 'add-client',
  templateUrl: 'add-client.component.html',
})

export class AddClientComponent implements AfterViewInit  { 

   client: any;
   isSaved = false;
   loader = false;
   message:any;

  constructor(private _clientService:ClientService){}

  @ViewChild('msgFocous') vc: any;

  ngAfterViewInit() {
    this.vc.nativeElement.focus();
  }
  // Save todo
  add(event:any, clientName:any){
      this.loader = true;
      var newClient = {
        name: clientName.value
      };

      this._clientService.save(newClient).subscribe(res => {
        this.isSaved = true;
        this.loader = false;
        setTimeout(function() {
            this.isSaved = false;
        }.bind(this), 3000);
    });
  }
 }