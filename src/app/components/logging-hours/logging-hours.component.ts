// Global Imports
import { Component, OnInit , ViewChild, AfterViewInit} from '@angular/core';
import 'rxjs/add/operator/map';

// Service Imports
import { SettingService } from '../../services/setting.service';

// Model Imports
import { Setting } from '../../setting';
@Component({
  moduleId: module.id,
  selector: 'logging-hours',
  templateUrl: 'logging-hours.component.html',
})
export class LoggingHoursComponent  implements OnInit, AfterViewInit{ 

  total_hours:any;
  isSaved = false;

  constructor(private _settingService:SettingService){}
   @ViewChild('msgFocous') vc: any;

  ngAfterViewInit() {
    this.vc.nativeElement.focus();
  }
  ngOnInit(){
    // get API ket
    this._settingService.get('total_hours')
    .subscribe(total_hours => {
      this.total_hours = total_hours[0].value;
    });
    
  }

  // Update 
  onSubmit(form:any){

    var updateTotalHours = {
        total_hours:     form.total_hours,
        option:          'total_hours'
      };
       this._settingService.update(updateTotalHours).subscribe(res => {
        this.isSaved = true;
        setTimeout(function() {
            this.isSaved = false;
        }.bind(this), 3000);
       });
  }
}