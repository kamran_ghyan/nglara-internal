import { Pipe, PipeTransform } from '@angular/core';

import { Employee } from '../../employee';

@Pipe({
    name: 'ClientFilter'
})

export class DropdownClientFilter implements PipeTransform {
    
    // Dropdown Filter
    transform(value: any, args: string[]): any {
       let filter = args;
       return filter ? value.filter((client: any)=> client.client.name.indexOf(filter) != -1) : value;
    }
}
