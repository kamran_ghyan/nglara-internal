import { Pipe, PipeTransform } from '@angular/core';

import { Employee } from '../../employee';

@Pipe({
    name: 'LocationFilter'
})

export class DropdownLocationFilter implements PipeTransform {
    
    // Dropdown Filter
    transform(value: any, args: string[]): any {
       let filter = args;
       return filter ? value.filter((location: any)=> location.location.name.indexOf(filter) != -1) : value;
    }
}
