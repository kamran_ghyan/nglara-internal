import { Pipe, PipeTransform } from '@angular/core';

import { Employee } from '../../employee';

@Pipe({
    name: 'DepartmentFilter'
})

export class DropdownDepartmentFilter implements PipeTransform {
    
    // Dropdown Filter
    transform(value: any, args: string[]): any {
       let filter = args;
       return filter ? value.filter((department: any)=> department.department.name.indexOf(filter) != -1) : value;
    }
}
