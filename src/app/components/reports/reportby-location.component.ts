// Global Imports
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/map';

// Service Imports
import { LocationService } from '../../services/location.service';
import { DepartmentService } from '../../services/department.service';
import { ClientService } from '../../services/client.service';
import { EmployeeService } from '../../services/employee.service';
import { SettingService } from '../../services/setting.service';
import { OverheadService } from '../../services/overhead.service';


// Model Imports
import { Location } from '../../location';
import { Department } from '../../department';
import { Client } from '../../client';
import { Employee } from '../../employee';
import { Setting } from '../../setting';

@Component({
  moduleId: module.id,
  selector: 'reports',
  templateUrl: 'reportby-location.component.html',
})
export class ReportsByLocationComponent implements OnInit{

  locations:any;
  departments:any;
  clients:any;
  crms:any;
  managers:any;
  public data:any;
  sumSalary:any;
  sumGross:any;
  sumNet:any;
  avgSalary:any;
  avgGross:any;
  avgNet:any;
  excahngeRate:any;
  totalHours:any;
  totalSum:any;
  public filterQuery = "";
  public rowsOnPage = 10;
  public sortBy = "name";
  public sortOrder = "desc";

  constructor(
    private _locationService:LocationService, 
    private _departmentService:DepartmentService, 
    private _clientService:ClientService,
    private _settingService:SettingService,
    private _overheadService:OverheadService,
    private route: ActivatedRoute, 
    private _employeeService:EmployeeService
  ){}

  

  ngOnInit(){

    
    // get SUM overhead
    this._overheadService.getSum()
    .subscribe(totalSum => {
      this.totalSum = parseInt(totalSum.total);
    });

    // get API ket
    this._settingService.get('api_key')
    .subscribe(exchangeRate => {
      this.excahngeRate = exchangeRate[0].value;
    });
    // get API secret
    this._settingService.get('total_hours')
    .subscribe(totalHours => {
      this.totalHours = totalHours[0].value;
    });

    // get all location
    this.locations = [];
    this._locationService.get()
    .subscribe(locations => {
      this.locations = locations.data;
    });
    // get all departments
    this.departments = [];
    this._departmentService.get()
    .subscribe(departments => {
      this.departments = departments.data;
    });

    // get all clients
    this.clients = [];
    this._clientService.get()
    .subscribe(clients => {
      this.clients = clients.data;
    });

    // get all crms
    this.crms = [];
    this._employeeService.getCrm()
    .subscribe(crms => {
      this.crms = crms;
    });

    // get all managers
    this.managers = [];
    this._employeeService.getManager()
    .subscribe(managers => {
      this.managers = managers;
    });

    // get all employee
    // this._employeeService.get()
    // .subscribe((allEmployee:any) => {
    //   this.data = allEmployee.data;
    //   this.sumSalary = this.calculateSalary(allEmployee.data);
    //   this.sumGross = this.calculateSalary(allEmployee.data);
    //   this.sumNet = this.calculateSalary(allEmployee.data); 
    // });

    // get emplye by location / type
    if(this.route.snapshot.params['overhead'] !== undefined){
    
    } else{
     
      this._employeeService.getEmpByLocationType(this.route.snapshot.params['id'], this.route.snapshot.params['type'])
    .subscribe((allEmployee:any) => {
      this.data = allEmployee.data;
      this.sumSalary = this.calculateSalary(allEmployee.data);
      this.sumGross = this.calculateSalary(allEmployee.data);
      this.sumNet = this.calculateSalary(allEmployee.data); 
    });
    }
    
    
  }

  // Edit state mode
    setEditState(employee:any, state:any){
        if(state == false){
            employee.isEditMode = false;
        } else {
            employee.isEditMode = true;
        }
    }

    // Update location
    updateSalary(event:any, location:any){
      if(event.which === 13){
        var _location = {
          id: location.id,
          salary: location.salary
        };

        this._employeeService.updateSalary(_location)
        .subscribe(data => {
          this.setEditState(location, false);
        });
      }
      
    }

  onSubmit(event:any){

    if(event.type == 'click'){
      localStorage.setItem('location', '');
      localStorage.setItem('department', '');
      localStorage.setItem('client', '');

    } else {
        var selectionName = event.target.name;
        var selectionValue = event.target.value;
        
        if(selectionName == 'location'){

          localStorage.setItem('location', '');

          if(selectionValue == '' || selectionValue == undefined){
            localStorage.setItem(selectionName, '');
          } else {
            localStorage.setItem(selectionName, selectionValue);
          }
          
        }
        if(selectionName == 'department'){

          localStorage.setItem('department', '');

        if(selectionValue == '' || selectionValue == undefined){
            localStorage.setItem(selectionName, '');
          } else {
            localStorage.setItem(selectionName, selectionValue);
          }
        }
        if(selectionName == 'client'){

          localStorage.setItem('client', '');

          if(selectionValue == '' || selectionValue == undefined){
            localStorage.setItem(selectionName, '');
          } else {
            localStorage.setItem(selectionName, selectionValue);
          }
        }
    }
    
    

     var search = {
        location: localStorage.getItem('location'),
        department: localStorage.getItem('department'),
        client: localStorage.getItem('client')
      };
    
    // get serached employees
    this.data = [];
    this._employeeService.getSearch(search)
    .subscribe(allEmployee => {
      this.data = allEmployee.data;
      this.sumSalary = this.calculateSalary(allEmployee.data);
      this.sumGross = this.calculateSalary(allEmployee.data);
      this.sumNet = this.calculateSalary(allEmployee.data);
    });
  }

  totalProfitLoss(a:any, b:any, c:any){
      return a - (b + c/this.excahngeRate);
    
  }
 calculateSalary(data:any) {
    
    let totalSalary = 0;
    let totalGross = 0;
    let totalNet = 0;
    let avgSalary = 0;
    let avgGross = 0;
    let avgNet = 0;


    for(let i=0; i < data.length; i++){
           totalSalary += data[i].salary / this.excahngeRate;
           totalGross += data[i].rate * data[i].hours_worked;
           totalNet += data[i].rate * data[i].hours_worked - data[i].salary / this.excahngeRate;
    }
    
    avgSalary = totalSalary/data.length;
    avgGross  = totalGross/data.length;
    avgNet    = totalNet/data.length;

   
    return {
      salary: totalSalary ,
      gross: totalGross,
      net: totalNet,
      avgsalary: avgSalary,
      avggross: avgGross,
      avgnet: avgNet
    };
  }

 }