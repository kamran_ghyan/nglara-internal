// Global Imports
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/map';

// Service Imports
import { LocationService } from '../../services/location.service';
import { DepartmentService } from '../../services/department.service';
import { ClientService } from '../../services/client.service';
import { EmployeeService } from '../../services/employee.service';
import { SettingService } from '../../services/setting.service';
import { OverheadService } from '../../services/overhead.service';
import { TypeService } from '../../services/type.service';


// Model Imports
import { Location } from '../../location';
import { Department } from '../../department';
import { Client } from '../../client';
import { Employee } from '../../employee';
import { Setting } from '../../setting';

@Component({
  moduleId: module.id,
  selector: 'reports',
  templateUrl: 'reports.component.html',
})
export class ReportsComponent implements OnInit{

  locations:any;
  departments:any;
  types:any;
  noFound = false;
  clients:any;
  crms:any;
  managers:any;
  public data:any [];
  public overhead:any [];
  sumSalary:any;
  sumGross:any;
  sumNet:any;
  avgSalary:any;
  avgGross:any;
  avgNet:any;
  excahngeRate:any;
  totalHours:any;
  totalSum:any;
  sumPKR:any;
  sumUSD:any;
  public filterQuery = "";
  public rowsOnPage = 10;
  public sortBy = "name";
  public sortOrder = "desc";
  excludeClass = false;
  toggleClass = false;
  allToggleState:any;
  loader = false;

  constructor(
    private _locationService:LocationService, 
    private _departmentService:DepartmentService, 
    private _clientService:ClientService,
    private _settingService:SettingService,
    private route: ActivatedRoute, 
    private _overheadService:OverheadService,
    private _typeService:TypeService,
    private _employeeService:EmployeeService
  ){}

  

  ngOnInit(): void{

    this.allToggleState = localStorage.getItem('toggleall');

    // get SUM overhead
    this._overheadService.getSum()
    .subscribe(totalSum => {
      this.totalSum = parseInt(totalSum.total);
    });

    // get API ket
    this._settingService.get('api_key')
    .subscribe(exchangeRate => {
      this.excahngeRate = exchangeRate[0].value;
    });
    // get API secret
    this._settingService.get('total_hours')
    .subscribe(totalHours => {
      this.totalHours = totalHours[0].value;
    });

    // get all location
    this.locations = [];
    this._locationService.get()
    .subscribe(locations => {
      this.locations = locations.data;
    });

    // get all departments
    this.departments = [];
    this._departmentService.get()
    .subscribe(departments => {
      this.departments = departments.data;
    });

    // get all types
    this.types = [];
    this._typeService.get()
    .subscribe(types => {
      this.types = types.data;
    });

    // get all clients
    this.clients = [];
    this._clientService.get()
    .subscribe(clients => {
      this.clients = clients.data;
    });

    // get all crms
    this.crms = [];
    this._employeeService.getCrm()
    .subscribe(crms => {
      this.crms = crms;
    });

    // get all managers
    this.managers = [];
    this._employeeService.getManager()
    .subscribe(managers => {
      this.managers = managers;
    });

    // get all employee
    //this.allEmployee = [];
    if(this.route.snapshot.params['overhead'] !== undefined){
        this._employeeService.getEmpByLocationType(this.route.snapshot.params['id'], this.route.snapshot.params['overhead'])
        .subscribe((allEmployee:any) => {
          this.overhead = allEmployee.data;
          this.sumPKR = this.calculateExpense(allEmployee.data);
          this.sumUSD = this.calculateExpense(allEmployee.data);
        });
    } else if(this.route.snapshot.params['type'] !== undefined){

        this._employeeService.getEmpByLocationType(this.route.snapshot.params['id'], this.route.snapshot.params['type'])
        .subscribe((allEmployee:any) => {
          this.data = allEmployee.data;
          if(this.data.length > 0 ){
            this.noFound = false;
          } else{
            this.noFound = true
          }
          this.sumSalary = this.calculateSalary(allEmployee.data);
          this.sumGross = this.calculateSalary(allEmployee.data);
          this.sumNet = this.calculateSalary(allEmployee.data); 
        });
    } else if(this.route.snapshot.params['bench'] !== undefined){

        this._employeeService.getEmpByLocationType(this.route.snapshot.params['id'], this.route.snapshot.params['bench'])
        .subscribe((allEmployee:any) => {
          this.data = allEmployee.data;
          this.sumSalary = this.calculateSalary(allEmployee.data);
          this.sumGross = this.calculateSalary(allEmployee.data);
          this.sumNet = this.calculateSalary(allEmployee.data); 
        });
    } else{
      this._employeeService.get()
      .subscribe((allEmployee:any) => {
        this.data = allEmployee.data;
        this.sumSalary = this.calculateSalary(allEmployee.data);
        this.sumGross = this.calculateSalary(allEmployee.data);
        this.sumNet = this.calculateSalary(allEmployee.data); 
        
      });
    }
    
  }

  // Edit state mode
  setEditState(employee:any, state:any){
      if(state == false){
          employee.isEditMode = false;
      } else {
          employee.isEditMode = true;
      }
  }

  rateEditState(employee:any, state:any){
    if(state == false){
        employee.rateEditMode = false;
    } else {
        employee.rateEditMode = true;
    }
  }

  // Update location
  updateSalary(event:any, location:any){
    if(event.which === 13){
      var _location = {
        id: location.id,
        salary: location.salary
      };

      this._employeeService.updateSalary(_location)
      .subscribe(data => {
        this.setEditState(location, false);
      });
    }
    
  }

    // Update rate
  updateRate(event:any, location:any){
    if(event.which === 13){
      var _location = {
        id: location.id,
        rate: location.rate
      };

      this._employeeService.updateRate(_location)
      .subscribe(data => {
        this.ngOnInit();
        this.rateEditState(location, false);
      });
     
    }
    
  }

  // Update location
  updateUsSalary(event:any, location:any){
    if(event.which === 13){
      var _location = {
        id: location.id,
        usdsalary: location.gross
      };
      console.log(_location)
      this._employeeService.updateSalary(_location)
      .subscribe(data => {
        this.setEditState(location, false);
      });
    }
    
  }

  // Update location
  updateExclude(event:any, location:any){
    
    if(location.exclude === true){
      this.excludeClass = true;
        var _location = {
        id: location.id,
        exclude: 1
      };
      } else {
        this.excludeClass = false;
        var _location = {
        id: location.id,
        exclude: 0
      };
      }

      this._employeeService.updateExclude(_location)
        .subscribe(data => {
          this.setEditState(location, false);
          this._employeeService.get()
        .subscribe((allEmployee:any) => {
          this.data = allEmployee.data;
          this.sumSalary = this.calculateSalary(allEmployee.data);
          this.sumGross = this.calculateSalary(allEmployee.data);
          this.sumNet = this.calculateSalary(allEmployee.data); 
        });   
      });
  }

  updateToggle(event:any, location:any){
    if(location.toggle === true){
      this.toggleClass = true;
        var _location = {
        id: location.id,
        toggle: 1
      };
      } else {
        this.toggleClass = false;
        var _location = {
        id: location.id,
        toggle: 0
      };
      } 
      this._employeeService.updateToggle(_location)
        .subscribe(data => {
          this.setEditState(location, false);   
      });
  }

  allToggle(event:any, location:any){
      this.loader = true;
      
      this._employeeService.allToggle()
        .subscribe(res => {

          this.ngOnInit();
          this.loader = false;
          //this.setEditState(location, false);
          
         localStorage.setItem('toggleall', res.data);

         this.allToggleState = localStorage.getItem('toggleall');
      });
  }

  onSubmit(event:any){

    if(event.type == 'click'){
      localStorage.setItem('location', '');
      localStorage.setItem('department', '');
      localStorage.setItem('client', '');

    } else {
        var selectionName = event.target.name;
        var selectionValue = event.target.value;
        
        if(selectionName == 'location'){

          localStorage.setItem('location', '');

          if(selectionValue == '' || selectionValue == undefined){
            localStorage.setItem(selectionName, '');
          } else {
            localStorage.setItem(selectionName, selectionValue);
          }
          
        }
        if(selectionName == 'department'){

          localStorage.setItem('department', '');

        if(selectionValue == '' || selectionValue == undefined){
            localStorage.setItem(selectionName, '');
          } else {
            localStorage.setItem(selectionName, selectionValue);
          }
        }
        if(selectionName == 'client'){

          localStorage.setItem('client', '');

          if(selectionValue == '' || selectionValue == undefined){
            localStorage.setItem(selectionName, '');
          } else {
            localStorage.setItem(selectionName, selectionValue);
          }
        }
    }
    
    

     var search = {
        location: localStorage.getItem('location'),
        type: localStorage.getItem('department'),
        client: localStorage.getItem('client')
      };
    
    // get serached employees
    this.data = [];
    this._employeeService.getSearch(search)
    .subscribe(allEmployee => {
      this.data = allEmployee.data;
      if(this.data.length > 0 ){
        this.noFound = false;
      } else{
        this.noFound = true
      }
      console.log(this.data);
      this.sumSalary = this.calculateSalary(allEmployee.data);
      this.sumGross = this.calculateSalary(allEmployee.data);
      this.sumNet = this.calculateSalary(allEmployee.data);
      console.log(this.sumGross);
    });
  }

  totalProfitLoss(a:any, b:any, c:any){
      return a - (b + c/this.excahngeRate);
  }

 calculateExpense(data:any){

    let totalPKR = 0;
    let totalUSD = 0;
    let avgPKR = 0;
    let avgUSD = 0;
  
    for(let i=0; i < data.length; i++){
           totalPKR += data[i].cost;
           totalUSD += data[i].cost / this.excahngeRate;
    }
    
    avgPKR = totalPKR/data.length;
    avgUSD  = totalUSD/data.length;

    return {
      expPKR: totalPKR ,
      expUSD: totalUSD,
      avgPKR: avgPKR,
      avgUSD: avgUSD,
    };

 }
 calculateSalary(data:any) {
    
    let totalSalary = 0;
    let totalGross = 0;
    let totalNet = 0;
    let avgSalary = 0;
    let avgGross = 0;
    let avgNet = 0;
    let totalEmp = 0;

    for(let i=0; i < data.length; i++){
           
           if(data[i].exclude == 1){
           } else {
             if(data[i].hours_worked == 0){
               if(data[i].salary == 0 || data[i].salary == null){
                  totalSalary += data[i].gross;
                  totalGross += 0;
                  totalNet += 0;
               } else {
                  totalSalary += data[i].salary / this.excahngeRate;
                  totalGross += data[i].rate * this.totalHours;
                  totalNet += data[i].rate * this.totalHours - data[i].salary / this.excahngeRate;
                  
               }
                
             } else {
               if(data[i].salary == 0 || data[i].salary == null){
                totalSalary += data[i].gross;
                totalGross += 0;
                totalNet += 0;
               } else {
                totalSalary += data[i].salary / this.excahngeRate;
                totalGross += data[i].rate * data[i].hours_worked;
                totalNet += data[i].rate * data[i].hours_worked - data[i].salary / this.excahngeRate;
               }
                
             }
            
           }

    }

    avgSalary = totalSalary/data.length;
    avgGross  = totalGross/data.length;
    avgNet    = totalNet/data.length;

   
    return {
      salary: totalSalary ,
      gross: totalGross,
      net: totalNet,
      avgsalary: avgSalary,
      avggross: avgGross,
      avgnet: avgNet
    };
  }

 }