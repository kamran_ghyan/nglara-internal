import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import 'rxjs/add/operator/map';

// Service Imports
import { LocationService } from '../../services/location.service';
import { DepartmentService } from '../../services/department.service';
import { ClientService } from '../../services/client.service';
import { EmployeeService } from '../../services/employee.service';
import { SettingService } from '../../services/setting.service';
import { TypeService } from '../../services/type.service';

// Model Imports
import { Location } from '../../location';
import { Department } from '../../department';
import { Client } from '../../client';
import { Employee } from '../../employee';
import { Setting } from '../../setting';
  
@Component({
  moduleId: module.id,
  selector: 'add-employee',
  templateUrl: 'add-employee.component.html',
})

export class AddEmployeeComponent implements OnInit, AfterViewInit  { 

   location: any;
   types:any;
   isSaved = false;
   loader = false;
   message:any;
   locations:any;
   departments:any;
   clients:any;
   crms:any;
   managers:any;

   exchangeRate:any;
   totalHours:any;

   showDefaultType = true;
   showCustomType = false;

   showUsSalary = false;
   

  constructor(
    private _locationService:LocationService, 
    private _departmentService:DepartmentService, 
    private _clientService:ClientService,
    private _settingService:SettingService,
    private _typeService:TypeService,
    private _employeeService:EmployeeService
  ){}

  @ViewChild('msgFocous') vc: any;

  ngAfterViewInit() {
    this.vc.nativeElement.focus();
  }


    ngOnInit(){

      this.showDefaultType = true;
      this.showCustomType = false;

    // get API ket
    this._settingService.get('api_key')
    .subscribe(exchangeRate => {
      this.exchangeRate = exchangeRate[0].value;
    });
    // get API secret
    this._settingService.get('total_hours')
    .subscribe(totalHours => {
      this.totalHours = totalHours[0].value;
    });
    
    // get all location
    this.locations = [];
    this._locationService.get()
    .subscribe(locations => {
      this.locations = locations.data;
    });

    // get all types
    this.types = [];
    this._typeService.get()
    .subscribe(types => {
      this.types = types.data;
    });

    // get all departments
    this.departments = [];
    this._departmentService.get()
    .subscribe(departments => {
      this.departments = departments.data;
    });

    // get all clients
    this.clients = [];
    this._clientService.get()
    .subscribe(clients => {
      this.clients = clients.data;
    });

    // get all crms
    this.crms = [];
    this._employeeService.getCrm()
    .subscribe(crms => {
      this.crms = crms;
    });

    // get all managers
    this.managers = [];
    this._employeeService.getManager()
    .subscribe(managers => {
      this.managers = managers;
    });

  }
  
  // Save 
  add(event:any, employeeName:any){

      var newEmployee = {
        name: employeeName.value
      }; 

     
  }

  // toggle
  toggle(event:any, hoursType:any){
    let type = hoursType.value;

    if(type == 0){
      this.showCustomType = true;
      this.showDefaultType = false; 
    } else {
      this.showCustomType = false;
      this.showDefaultType = true;
    }
  } 

  // toggle
  toggleSalary(event:any, expenseType:any){

    let type = expenseType.checked;
    if(type == true){
      this.showUsSalary = true;
    } else {
      this.showUsSalary = false;
    }
  } 

  // Save 
  onSubmit(form:any){
    this.loader = true;
    if(form.totalHoursDefault == true || form.totalHours == '' || form.totalHours == undefined){
      var newTotalHours = 0;
    } else {
      newTotalHours = form.totalHours;
    }
    console.log(form)

    var newEmployee = {}

      if(form.usdsalary == undefined){
         newEmployee = {
            name:           form.name,
            salary:         form.salary,
            gross:          form.gross,
            net:            form.net,
            rate:           form.rate,
            emp_type:       form.typeEmp,
            location_id:    form.location,
            // department_id:  form.department,
            client_id:      form.client,
            notes:          form.notes,
            hours_worked:   newTotalHours
          };
        } else {
          newEmployee = {
            name:           form.name,
            usdsalary:      form.usdsalary,
            gross:          form.gross,
            net:            form.net,
            rate:           form.rate,
            emp_type:       form.typeEmp,
            location_id:    form.location,
            // department_id:  form.department,
            client_id:      form.client,
            notes:          form.notes,
            hours_worked:   newTotalHours
          };
        }
    

       this._employeeService.save(newEmployee).subscribe(res => {
        this.isSaved = true;
        this.loader = false;
        setTimeout(function() {
            this.isSaved = false;
            //this.form.reset();
        }.bind(this), 3000);
       });
  }
 }