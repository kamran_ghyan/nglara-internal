// Global Imports
import { Component, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';

// Service Imports
import { EmployeeService } from '../../services/employee.service';
import { SettingService } from '../../services/setting.service';

// Model Imports
import { Employee } from '../../employee';

@Component({
  moduleId: module.id,
  selector: 'employee',
  templateUrl: 'employee.component.html',
})

export class EmployeeComponent implements OnInit  {

  data: any;
  isSaved = false;
  excahngeRate:any;
  updatedValue:string;

  constructor(
    private _employeeService:EmployeeService,
    private _settingService:SettingService
    ){}

  ngOnInit(){

    // get API ket
    this._settingService.get('api_key')
    .subscribe(exchangeRate => {
      this.excahngeRate = exchangeRate[0].value;
    });

    this.data = [];
    this._employeeService.get()
    .subscribe(employees => {
      this.data = employees.data;
    });
  }

  // Edit state mode
    setEditState(employee:any, state:any, index:any){
        if(state){
             
              employee.isEditMode = false;
              employee.isEditMode = state;
            
        } else {
            delete employee.isEditMode;
        }
    }

    // Update 
    update(event:any, employee:any, state:any, index:any){

         if(event.which === 13){
            var _employee = {
              id: employee.id,
              name: employee.salary
            };
         }

        this._employeeService.update(employee)
        .subscribe(data => {
          this.setEditState(employee, !state, index);
        });
      
    }

    updateSalary(event:any, employee:any, state:any, index:any){
    if(event.which === 13){
      var _employee = {
        id: employee.id,
        salary: employee.salary
      };

      this._employeeService.updateSalary(_employee)
      .subscribe(data => {
        this.setEditState(employee, !state, index);
      });
    }
    
  }

  updateUsSalary(event:any, employee:any, state:any, index:any){
    if(event.which === 13){
      var _employee = {
        id: employee.id,
        usdsalary: employee.gross
      };

      this._employeeService.updateSalary(_employee)
      .subscribe(data => {
        this.setEditState(employee, !state, index);
      });
    }
    
  }

  // Delete 
    delete(id:any){
      if (confirm("Are you sure you want to delete this Employee?")) {
        this._employeeService.delete(id)
        .subscribe(data => {

            this.ngOnInit();

            this.isSaved = true;
            setTimeout(function() {
                this.isSaved = false;
            }.bind(this), 3000);

          });
      }
      
    }
  
 }