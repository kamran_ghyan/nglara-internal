import { Component, OnInit, ViewChild, AfterViewInit  } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/map';

// Service Imports
import { LocationService } from '../../services/location.service';
import { DepartmentService } from '../../services/department.service';
import { ClientService } from '../../services/client.service';
import { EmployeeService } from '../../services/employee.service';
import { SettingService } from '../../services/setting.service';
import { TypeService } from '../../services/type.service';

// Model Imports
import { Location } from '../../location';
import { Department } from '../../department';
import { Client } from '../../client';
import { Employee } from '../../employee';
import { Setting } from '../../setting';
  
@Component({
  moduleId: module.id,
  selector: 'edit-employee',
  templateUrl: 'edit-employee.component.html',
})

export class EditEmployeeComponent implements OnInit, AfterViewInit  { 

   employee: any;
   types: any;
   isSaved = false;
   isDeleted = false;
   loader = false;
   message:any;
   locations:any;
   departments:any;
   clients:any;
   crms:any;
   managers:any;

   exchangeRate:any;
   totalHours:any;

   showDefaultType = true;
   showCustomType = false;

   showUsSalary = false;
   

  constructor(
    private _locationService:LocationService, 
    private _departmentService:DepartmentService, 
    private _clientService:ClientService,
    private route: ActivatedRoute, 
    private router: Router,
    private _typeService:TypeService,
    private _settingService:SettingService,
    private _employeeService:EmployeeService
  ){}

   @ViewChild('msgFocous') vc: any;

  ngAfterViewInit() {
    this.vc.nativeElement.focus();
  }


    ngOnInit(){
    
    // get API ket
    this._settingService.get('api_key')
    .subscribe(exchangeRate => {
      this.exchangeRate = exchangeRate[0].value;
    });
    // get API secret
    this._settingService.get('total_hours')
    .subscribe(totalHours => {
      this.totalHours = totalHours[0].value;
    });
    
    // get employee
    this.employee = [];
    this._employeeService.getEmp(this.route.snapshot.params['id'])
    .subscribe(employee => {
      this.employee = employee[0];
      if(this.employee.hours_worked == 0){
        this.showDefaultType = true;
        this.showCustomType = false;
      } else{
        this.showDefaultType = false;
        this.showCustomType = true;
      }

      if(this.employee.gross == 0){
        this.showUsSalary = false;
      } else {
        this.showUsSalary = true;
      }
    });

    // get all location
    this.locations = [];
    this._locationService.get()
    .subscribe(locations => {
      this.locations = locations.data;
    });

    // get all types
    this.types = [];
    this._typeService.get()
    .subscribe(types => {
      this.types = types.data;
    });

    // get all departments
    this.departments = [];
    this._departmentService.get()
    .subscribe(departments => {
      this.departments = departments.data;
    });

    // get all clients
    this.clients = [];
    this._clientService.get()
    .subscribe(clients => {
      this.clients = clients.data;
    });

    // get all crms
    this.crms = [];
    this._employeeService.getCrm()
    .subscribe(crms => {
      this.crms = crms;
    });

    // get all managers
    this.managers = [];
    this._employeeService.getManager()
    .subscribe(managers => {
      this.managers = managers;
    });

  }
  
  // Save 
  add(event:any, employeeName:any){

      var newEmployee = {
        name: employeeName.value
      }; 

     
  }

  // toggle
  toggle(event:any, hoursType:any){
    let type = hoursType.value;

    if(type == 0){
      this.showCustomType = true;
      this.showDefaultType = false; 
    } else {
      this.showCustomType = false;
      this.showDefaultType = true;
    }
  } 

  // toggle
  toggleSalary(event:any, expenseType:any){

    let type = expenseType.checked;
    if(type == true){
      this.showUsSalary = true;
    } else {
      this.showUsSalary = false;
    }
  } 

  // Save 
  onSubmit(form:any){
    this.loader = true;
    if(form.totalHoursDefault == true || form.totalHours == '' || form.totalHours == undefined){
      var newTotalHours = 0;
    } else {
      newTotalHours = form.totalHours;
    }

    var newEmployee = {}

      if(form.usdsalary == undefined){
          newEmployee = {
            id:             form.id,
            name:           form.name,
            notes:          form.notes,
            salary:         form.salary,
            rate:           form.rate,
            emp_type:       form.empType,
            location_id:    form.location,
            // department_id:  form.department,
            client_id:      form.client,
            hours_worked:   newTotalHours
          }; 
        } else {
         newEmployee = {
            id:             form.id,
            name:           form.name,
            notes:          form.notes,
            usdsalary:      form.usdsalary,
            rate:           form.rate,
            emp_type:       form.empType,
            location_id:    form.location,
            // department_id:  form.department,
            client_id:      form.client,
            hours_worked:   newTotalHours
          };  
        }

        

       this._employeeService.update(newEmployee).subscribe(res => {
        this.isSaved = true;
        this.loader = false;
        setTimeout(function() {
            this.isSaved = false;
        }.bind(this), 3000);
       });
  }

  // Delete 
    delete(id:any){
      if (confirm("Are you sure you want to delete this Employee?")) {
        this._employeeService.delete(id)
        .subscribe(data => {
            this.isDeleted = true;
            setTimeout(function() {
                this.isDeleted = false;
                this.router.navigate(['./employee']);
            }.bind(this), 3000);

          });
      }
      
    }

 }