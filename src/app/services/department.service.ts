import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class DepartmentService{

    // API URL
    api_url = 'https://psm.allshoreresources.com';
    //api_url = 'http://localhost:8888';

    constructor(private _http:Http ){}

    // Get listing
    get(){
        return this._http.get(this.api_url+'/api/v1/department')
        .map(res => res.json())
    }

    // Save 
    save(department:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post(this.api_url+'/api/v1/department', JSON.stringify(department), {headers:headers})
        .map(res => res.json())
    }

    // Update 
    update(department:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.put(this.api_url+'/api/v1/department/'+department.id, JSON.stringify(department), {headers:headers})
        .map(res => res.json())
    }
    
    // Delete 
    delete(id:any){
        return this._http.delete(this.api_url+'/api/v1/department/'+id)
        .map(res => res.json())
    }
}