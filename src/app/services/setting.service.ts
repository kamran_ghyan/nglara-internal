import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class SettingService{

    // API URL
    api_url = 'https://psm.allshoreresources.com';
    //api_url = 'http://localhost:8888';

    constructor(private _http:Http ){}

    // Get list
    get(option:any){
        return this._http.get(this.api_url+'/api/v1/setting/'+option)
        .map(res => res.json());
    }

   // Update 
    update(form:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.put(this.api_url+'/api/v1/setting/1', form)
        .map( res =>{ res.json() });
    }

    
}