import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class OverheadService{

    // API URL
    // local URL
    api_url = 'https://psm.allshoreresources.com';
    //api_url = 'http://localhost:8888';

    constructor(private _http:Http ){}

    // Get listing
    get(){
        return this._http.get(this.api_url+'/api/v1/overhead')
        .map(res => res.json())
    }

    // Save 
    save(overhead:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post(this.api_url+'/api/v1/overhead', JSON.stringify(overhead), {headers:headers})
        .map(res => {
            res.json();
        })
    }

    // Update 
    update(overhead:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.put(this.api_url+'/api/v1/overhead/'+overhead.id, JSON.stringify(overhead), {headers:headers})
        .map(res => res.json())
    }
    
    // Delete 
    delete(id:any){
        return this._http.delete(this.api_url+'/api/v1/overhead/'+id)
        .map(res => res.json())
    }

    // Get SUM
    getSum(){
        return this._http.get(this.api_url+'/api/v1/overhead/sum')
        .map(res => res.json())
    }

     // Get Overhead
    getOverhead(id:any){
        return this._http.get(this.api_url+'/api/v1/overhead/'+id+'/edit')
        .map(res => res.json())
    }

    // Get listing
    getSearch(form:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post(this.api_url+'/api/v1/overhead/search', JSON.stringify(form), {headers:headers})
        .map(res => res.json())
    }

    
}