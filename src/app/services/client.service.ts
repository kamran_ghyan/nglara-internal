import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class ClientService{

    // API URL
    // local URL
    api_url = 'https://psm.allshoreresources.com';
    //api_url = 'http://localhost:8888';
    

    constructor(private _http:Http ){}

    // Get list
    get(){            
        return this._http.get(this.api_url+'/api/v1/client')
        .map(res => res.json())
    }

    // Save 
    save(client:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post(this.api_url+'/api/v1/client', JSON.stringify(client), {headers:headers})
        .map(res => res.json())
    }

    // Update 
    update(client:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.put(this.api_url+'/api/v1/client/'+client.id, JSON.stringify(client), {headers:headers})
        .map(res => res.json() );
    }
    
    // Delete 
    delete(id:any){
        return this._http.delete(this.api_url+'/api/v1/client/'+id)
        .map(res => res.json())
    }
}