import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class LocationService{

    // API URL
    api_url = 'https://psm.allshoreresources.com';
    //api_url = 'http://localhost:8888';

    constructor(private _http:Http ){}

    // Get listing
    get(){
        return this._http.get(this.api_url+'/api/v1/location')
        .map(res => res.json())
    }

    // Get listing
    dashboard(){
        return this._http.get(this.api_url+'/api/v1/location/dashboard')
        .map(res => res.json())
    }

    // Save 
    save(location:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post(this.api_url+'/api/v1/location', JSON.stringify(location), {headers:headers})
        .map(
            res => {
                res.json()
            } );
    }

    // Update 
    update(location:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.put(this.api_url+'/api/v1/location/'+location.id, JSON.stringify(location), {headers:headers})
        .map(res =>{
             res.json()
             
        });
    }
    
    // Delete 
    delete(id:any){

        console.log(id);
        return this._http.delete(this.api_url+'/api/v1/location/'+id)
        .map(res =>{
             res.json();
             console.log(res);
        });
    }
}