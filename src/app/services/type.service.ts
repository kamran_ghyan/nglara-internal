import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class TypeService{

    // API URL
    api_url = 'https://psm.allshoreresources.com';
    //api_url = 'http://localhost:8888';

    constructor(private _http:Http ){}

    // Get list
    get(){
        return this._http.get(this.api_url+'/api/v1/type')
        .map(res => res.json())
    }

    // Save 
    save(type:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post(this.api_url+'/api/v1/type', JSON.stringify(type), {headers:headers})
        .map(res => res.json())
    }

    // Get listing
    getType(id:any){
        return this._http.get(this.api_url+'/api/v1/type/'+id+'/edit')
        .map(res => res.json())
    }

    // Update 
    update(client:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.put(this.api_url+'/api/v1/type/'+client.id, JSON.stringify(client), {headers:headers})
        .map(res => res.json() );
    }
    
    // Delete 
    delete(id:any){
        return this._http.delete(this.api_url+'/api/v1/type/'+id)
        .map(res => res.json())
    }
}