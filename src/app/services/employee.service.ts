import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class EmployeeService{

    // API URL
    // local URL
    api_url = 'https://psm.allshoreresources.com';
    //api_url = 'http://localhost:8888';

    constructor(private _http:Http ){}

    // Get listing
    get(){
        return this._http.get(this.api_url+'/api/v1/employee')
        .map(res => res.json())
    }

    // Get listing
    getSearch(form:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post(this.api_url+'/api/v1/employee/search', JSON.stringify(form), {headers:headers})
        .map(res => res.json())
    }

    // Get listing
    getEmp(id:any){
        return this._http.get(this.api_url+'/api/v1/employee/'+id+'/edit')
        .map(res => res.json())
    }

    // Get listing
    getType(id:any){
        return this._http.get(this.api_url+'/api/v1/employee/'+id+'/type')
        .map((res) => res.json())
    }

    // Get listing
    getEmpByLocation(id:any){
        return this._http.get(this.api_url+'/api/v1/employee/'+id+'/location')
        .map((res) => res.json())
    }

    // Get listing
    getEmpByLocationType(location_id:any, type_id:any){
        return this._http.get(this.api_url+'/api/v1/employee/'+location_id+'/location/'+type_id+'/type')
        .map((res) => res.json())
    }

    
    // Get listing
    getSumByLocation(id:any){
        return this._http.get(this.api_url+'/api/v1/overhead/'+id+'/sum')
        .map((res) => res.json())
    }

    // Save 
    save(employee:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post(this.api_url+'/api/v1/employee', JSON.stringify(employee), {headers:headers})
        .map(res => {
            res.json();
        })
    }

    // Update 
    update(employee:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.put(this.api_url+'/api/v1/employee/'+employee.id, JSON.stringify(employee), {headers:headers})
        .map(res => res.json())
    }

    // Update Salary
    updateSalary(employee:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.put(this.api_url+'/api/v1/employee/'+employee.id+'/salary', JSON.stringify(employee), {headers:headers})
        .map(res => res.json())
    }

    // Update Rate
    updateRate(employee:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.put(this.api_url+'/api/v1/employee/'+employee.id+'/rate', JSON.stringify(employee), {headers:headers})
        .map(res => res.json())
    }

    updateExclude(employee:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.put(this.api_url+'/api/v1/employee/'+employee.id+'/exclude', JSON.stringify(employee), {headers:headers})
        .map(res => res.json())
    }

    updateToggle(employee:any){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.put(this.api_url+'/api/v1/employee/'+employee.id+'/toggle', JSON.stringify(employee), {headers:headers})
        .map(res => res.json())
    }

    allToggle(){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post(this.api_url+'/api/v1/employee/alltoggle', {headers:headers})
        .map(res => res.json())
    }
    
    // Delete 
    delete(id:any){
        return this._http.delete(this.api_url+'/api/v1/employee/'+id)
        .map(res => res.json())
    }

    // Get CRM listing
    getCrm(){
        return this._http.get(this.api_url+'/api/v1/employee/crm')
        .map(res => res.json())
    }

    // Get Manager listing
    getManager(){
        return this._http.get(this.api_url+'/api/v1/employee/manager')
        .map(res => res.json())
    }

    employeeExport(){
         return this._http.get(this.api_url+'/api/v1/employee/export')
        .map(res => res.json())
    }

    employeeReset(){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
         return this._http.post(this.api_url+'/api/v1/employee/reset', '',headers)
        .map(res => res.json())
    }
}