// Global Imports
import { Component } from '@angular/core';

// Service Imports
import { ClientService } from './services/client.service';
import { DepartmentService } from './services/department.service';
import { EmployeeService } from './services/employee.service';
import { TypeService } from './services/type.service';
import { OverheadService } from './services/overhead.service';
import { LocationService } from './services/location.service';
import { SettingService } from './services/setting.service';


@Component({
  selector: 'my-app',
  templateUrl: './app/app.component.html',
  providers:[
    LocationService, 
    ClientService, 
    DepartmentService, 
    EmployeeService, 
    TypeService, 
    OverheadService, 
    SettingService
    ]
})

export class AppComponent  { }
